% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

clear all;
noise1=load('03-041300-canal1.txt');
noise2=load('03-041300-canal2.txt');
tsample=1.0/22222.222222;

noise1=noise1-mean(noise1);
noise2=noise2-mean(noise2);

nwin=2^17;
Fsamp=1.0/tsample; fn=Fsamp/2.;

[ccm,ww]=mscohere(noise1,noise2,hanning(nwin),nwin/2,nwin,Fsamp);
h1 = figure(); plot(ww,ccm); axis([0 120 -1 1])

%[ccm,ww]=cpsd(noise1,noise2,hanning(nwin),nwin/2,nwin,Fsamp);
%h2 = figure(); plot(ww,abs(ccm).^2);

%[ccm,ww]=pwelch(noise1,hanning(nwin),nwin/2,nwin,Fsamp);
%h3 = figure(); plot(ww,ccm);

%[ccm,ww]=pwelch(noise2,hanning(nwin),nwin/2,nwin,Fsamp);
%h4 = figure(); plot(ww,ccm);