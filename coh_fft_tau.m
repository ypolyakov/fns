% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function [m,Q12] = coh_cc_tau(T,TStart,DeltaT,M1,M2,nTheta2D,type);

%Loading the data of the first file into a matrix
V1=dlmread(M1,'\t',0,0);
V2=dlmread(M2,'\t',0,0);

%gets each DeltaT records
if DeltaT > 1
   Vtmp1 = V1(1:DeltaT:size(V1,1),:); 
   clear V1;
   V1 = Vtmp1;
   clear Vtmp1;
   Vtmp2 = V2(1:DeltaT:size(V2,1),:); 
   clear V2;
   V2 = Vtmp2;
   clear Vtmp2;
end

N = floor(T/DeltaT);
NStart = floor(TStart/DeltaT);
NTheta2D = floor(nTheta2D/DeltaT);

pN = N/2;

len = min([length(V2) length(V1)]);
i = 0;

maxi = floor((len-NStart)/(N/2))-1;
%denom1 = zeros(maxi,pN+1);
%denom2 = zeros(maxi,pN+1);
%num = zeros(maxi,pN+1);
sumnum = 0;
sumdenom1 = 0;
sumdenom2 = 0;

if type == 0
    win = rectwin(N);
else
    win = hamming(N);
end

for i=0:maxi-1 

    nMin = NStart + 1 + i.*N/2;
    nMax = NStart + i.*N/2 + N;
    
    a = V1(nMin:nMax,2);
    b = V2(nMin+NTheta2D:nMax+NTheta2D,2);
    
    a = a-mean(V1(nMin:nMax,2));
    b = b-mean(V2(nMin+NTheta2D:nMax+NTheta2D,2));

    PS = fft(win.*a);    
    PS = PS(1:(floor(length(PS)/2)+1));
    PS(2:length(PS)-1) = 2*PS(2:length(PS)-1);
    denom1 = PS;    
    sumdenom1 = sumdenom1 + abs(denom1).^2;
    
    PS = fft(win.*b);    
    PS = PS(1:(floor(length(PS)/2)+1));
    PS(2:length(PS)-1) = 2*PS(2:length(PS)-1);        
    denom2 = PS;    
    sumdenom2 = sumdenom2 + abs(denom2).^2;    

    sumnum = sumnum + denom1.*conj(denom2);
    
    %denom1(i+1,:) = abs(denom1(i+1,:)).^2;
    %denom2(i+1,:) = abs(denom2(i+1,:)).^2;
    
end

fs = 1./(V1(2,1)-V1(1,1));

Q12 = abs(sumnum).^2./(sumdenom1.*sumdenom2);    
%Q12 = abs(sum(num)).^2./(sum(denom1).*sum(denom2));     

m=0:1/(length(Q12)-1)*0.5.*fs/DeltaT:0.5.*fs;

