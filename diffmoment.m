% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function [m, DM, sigm] = diffmoment(T,TauMax,TStart,DeltaT,Ord,type,M1,nGap,nPercentGap);

%----------------------------DIFFMOMENT------------------------------------
%Ord - difference moment order
%type - type of difference moment
%    'cumulant'
%    'regular'
%T - averaging interval;
%TauMax - tau range;
%Tstart - start time;
%DeltaT - step size;
%M1 - file name and path;
%--------------------------------------------------------------------------

%Loading the data of the first file into a matrix
V=dlmread(M1,'\t',0,0);

%gets each DeltaT records
if DeltaT > 1
   Vtmp = V(1:DeltaT:size(V,1),:); 
   clear V;
   V = Vtmp;
   clear Vtmp;
end

N = floor(T/DeltaT);
NTau = floor(TauMax/DeltaT);
NStart = floor(TStart/DeltaT);
NMin = 1+NStart;
DM=zeros(1,NTau);

switch type
    case 0
        for nTau = 0:NTau-1
            
                if nGap == -1
                    DM(nTau+1) = 1/(N-nTau)* sum( (V(NMin:(N-nTau+NStart),2)-V((NMin+nTau):(N+NStart),2)).^Ord );
                else    
                    Tmp1 = 0;
                    i = 0;
                    for n = 1:(N-nTau)                   
                        if (V(n+NStart,2) == nGap) | (V(n+nTau+NStart,2) == nGap)
                            i = i + 1;
                        else
                            Tmp1 = Tmp1 + (V(n+NStart,2)-V(n+nTau+NStart,2)).^Ord;
                        end  
                    end
                    if i > nPercentGap./100.*(N-nTau)
                        DM(nTau+1) = 0;                    
                    else
                        DM(nTau+1) = 1/(N-nTau-i)*Tmp1;                                      
                    end
                end

        end 
    case 1
        for nTau = 0:NTau-1   
                if nGap == -1
                    df1 = 1/(N-nTau)* sum( (V(NMin:(N-nTau+NStart),2)-V((NMin+nTau):(N+NStart),2)).^Ord );
                    df2 = 1/(N-nTau)* sum( (V(NMin:(N-nTau+NStart),2)-V((NMin+nTau):(N+NStart),2)).^2 );
                else
                    df1 = 0;
                    df2 = 0;
                    i = 0;
                    for n = 1:(N-nTau)                   
                        if (V(n+NStart,2) == nGap) | (V(n+nTau+NStart,2) == nGap)
                            i = i + 1;
                        else
                            df1 = df1 + (V(n+NStart,2)-V(n+nTau+NStart,2)).^Ord;
                            df2 = df2 + (V(n+NStart,2)-V(n+nTau+NStart,2)).^2;
                        end  
                    end
                    if i > nPercentGap./100.*(N-nTau)   
                        df1 = 0;
                        df2 = 0;
                    else
                        df1 = 1/(N-nTau-i)*df1;                         
                        df2 = 1/(N-nTau-i)*df2;                      
                    end                
                end
                if isequal(df2,0)
                    DM(nTau+1) = 0;
                else
                    DM(nTau+1) = df1/df2.^(Ord/2);
                end
        end          
end

m=0:DeltaT:(NTau-1)*DeltaT;
sigm=std(V((1+NStart):(N+NStart),2),1);

    