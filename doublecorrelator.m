% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function [m,n,Q12,mTrace,IntParams] = doublecorrelator(T,TauMax,ThetaMax,TStart,DeltaT,M1,M2,nGap,nPercentGap,nNorm,nTauTrace,nThetaTrace,nTrace,nTheta2D,nThetaHalfWidth);
%----------------------------DOUBLECORRELATOR------------------------------
%doublecorrelator calculates double correlators
%T - averaging interval;
%ThetaMax - theta range;
%TauMax - tau range;
%Tstart - start time;
%DeltaT - step size;
%M1 - name of the first file
%M2 - name of the first file
%--------------------------------------------------------------------------

%Loading the data of the first file into a matrix
V1=dlmread(M1,'\t',0,0);
V2=dlmread(M2,'\t',0,0);

%gets each DeltaT records
if DeltaT > 1
   Vtmp1 = V1(1:DeltaT:size(V1,1),:); 
   clear V1;
   V1 = Vtmp1;
   clear Vtmp1;
   Vtmp2 = V2(1:DeltaT:size(V2,1),:); 
   clear V2;
   V2 = Vtmp2;
   clear Vtmp2;
end

N = floor(T/DeltaT);
NTau = floor(TauMax/DeltaT);
NTheta = floor(ThetaMax/DeltaT);
NStart = floor(TStart/DeltaT);
NMin = 1+NStart;
Q12=zeros(2*NTheta+1,NTau);
mTrace = zeros(NTau,2);

if nGap == -1

    for nTau = 1:NTau
            sigm1 =  sqrt( sum( (V1(NMin:(N-nTau+NStart),2)-V1((NMin+nTau):(N+NStart),2)).^2 ) );
            sigm2 =  sqrt( sum( (V2(NMin:(N-nTau+NStart),2)-V2((NMin+nTau):(N+NStart),2)).^2 ) ); 
            %sigm1 = sqrt( diffmomentSub(V1(NMin:N+NStart),N,nTau,2,0) );
            %sigm2 = sqrt( diffmomentSub(V2(NMin:N+NStart),N,nTau,2,0) );
            if isequal(sigm1,0) || isequal(sigm2,0)
                Q12(1:(2*NTheta+1),nTau) = 0     
            else
                for nTheta = -NTheta:NTheta
                    if nTheta < 0 
                        nMin = 1+abs(nTheta)+NStart;
                        nMax = N-nTau+NStart;
                    else
                        nMin = 1+NStart;
                        nMax = N-nTau-abs(nTheta)+NStart;
                    end
                    if nNorm == 1
                        sigm1 =  sqrt( sum( (V1(nMin:nMax,2)-V1((nMin+nTau):(nMax+nTau),2)).^2 ) );
                        sigm2 =  sqrt( sum( (V2((nMin+nTheta):(nMax+nTheta),2)-V2((nMin+nTau+nTheta):(nMax+nTau+nTheta),2)).^2 ) ); 
                    end
                    correl = sum( (V1(nMin:nMax,2)-V1((nMin+nTau):(nMax+nTau),2)).*...
                        (V2((nMin+nTheta):(nMax+nTheta),2)-V2((nMin+nTau+nTheta):(nMax+nTau+nTheta),2)) );
                                        
                    if nTrace == 1
                        % tracing functionality
                            if nTau == nTauTrace && nTheta == nThetaTrace
                                if isequal(sigm1,0) || isequal(sigm2,0)
                                    % keep mTrace at zeroes
                                else
                                    for n = 0:(nMax-nMin) 
                                        mTrace(n+1,1) = nMin + n;
                                        mTrace(n+1,2) = ((V1(n+nMin,2)-V1(n+nTau+nMin,2)).*(V2(n+nMin+nTheta,2)-V2(n+nTau+nMin+nTheta,2)))/(sigm1.*sigm2); 
                                    end
                                end
                            end
                        % end of tracing
                    end
                    
                    if nNorm == 1
                        if isequal(sigm1,0) || isequal(sigm2,0)
                            Q12(1:(2*NTheta+1),nTau) = 0   
                        else
                            Q12(nTheta+NTheta+1,nTau) = correl/(sigm1.*sigm2);
                        end
                    else
                        Q12(nTheta+NTheta+1,nTau) = (N-nTau)/(N-nTau-abs(nTheta))*correl/(sigm1.*sigm2);   
                    end
                end
            end
    end 
    
else

    for nTau = 1:NTau    
            Tmp1 = 0;
            i = 0;
            for n = 1:(N-nTau)                   
                if (V1(n+NStart,2) == nGap) | (V1(n+nTau+NStart,2) == nGap)
                    i = i + 1;
                else
                    Tmp1 = Tmp1 + (V1(n+NStart,2)-V1(n+nTau+NStart,2)).^2;
                end  
            end
            if i > nPercentGap./100.*(N-nTau)  
                sigm1 = 0;                   
            else
                sigm1 = sqrt( 1/(N-nTau-i)*Tmp1 ); 
            end
            
            Tmp1 = 0;
            i = 0;
            for n = 1:(N-nTau)                   
                if (V2(n+NStart,2) == nGap) | (V2(n+nTau+NStart,2) == nGap)
                    i = i + 1;
                else
                    Tmp1 = Tmp1 + (V2(n+NStart,2)-V2(n+nTau+NStart,2)).^2;
                end  
            end            
            if i > nPercentGap./100.*(N-nTau)  
                sigm2 = 0;                   
            else
                sigm2 = sqrt( 1/(N-nTau-i)*Tmp1 );       
            end
            
            if isequal(sigm1,0) || isequal(sigm2,0)
                Q12(1:(2*NTheta+1),nTau) = 0;     
            else
                for nTheta = -NTheta:NTheta
                    if nTheta < 0 
                        nMin = 1+abs(nTheta)+NStart;
                        nMax = N-nTau+NStart;
                    else
                        nMin = 1+NStart;
                        nMax = N-nTau-abs(nTheta)+NStart;
                    end
                                                         
                    Tmp1 = 0;
                    i = 0;
                    sigmtmp1 = 0;
                    sigmtmp2 = 0;
                    for n = 0:(nMax-nMin)                   
                        if (V1(n+nMin,2) == nGap) | (V1(n+nTau+nMin,2) == nGap) |...
                                (V2(n+nMin+nTheta,2) == nGap) | (V2(n+nTau+nMin+nTheta,2) == nGap)
                            i = i + 1;
                        else
                            if nNorm == 1
                                sigmtmp1 = sigmtmp1 + (V1(n+nMin,2)-V1(n+nMin+nTau,2)).^2;
                                sigmtmp2 = sigmtmp2 + (V2(n+nMin+nTheta,2)-V2(n+nMin+nTheta+nTau,2)).^2;
                            end
                            Tmp1 = Tmp1 + (V1(n+nMin,2)-V1(n+nTau+nMin,2)).*(V2(n+nMin+nTheta,2)-V2(n+nTau+nMin+nTheta,2));
                        end  
                    end
                    if nNorm == 1
                        sigm1 = sigmtmp1;
                        sigm2 = sigmtmp2;
                    end
                    correl = Tmp1;                                        
                    
                    if i > nPercentGap./100.*(N-nTau-abs(nTheta))
                        Q12(nTheta+NTheta+1,nTau) = 0;
                    else
                        if nNorm == 1
                            if isequal(sigm1,0) || isequal(sigm2,0)
                                Q12(1:(2*NTheta+1),nTau) = 0   
                            else
                                Q12(nTheta+NTheta+1,nTau) = correl/(sqrt(sigm1).*sqrt(sigm2));
                            end
                        else                          
                            Q12(nTheta+NTheta+1,nTau) = 1/(N-nTau-abs(nTheta)-i)*correl/(sigm1.*sigm2);
                        end
                    end
                    
                end
            end
            
    end
    
end

Qantinegtheta = 0;
Qcorrnegtheta = 0;
Qantipostheta = 0;
Qcorrpostheta = 0;
Qantizerotheta = 0;
Qcorrzerotheta = 0;

%antithetacount = 0;
%corrthetacount = 0;

for p = 1:NTheta
    for q = 1:NTau
        if Q12(p,q) < 0
            Qantinegtheta = Qantinegtheta + Q12(p,q);
            %antithetacount = antithetacount + 1;
        elseif Q12(p,q) > 0
            Qcorrnegtheta = Qcorrnegtheta + Q12(p,q);
            %corrthetacount = corrthetacount + 1;
        end 
    end
end

%if antithetacount > 0
Qantinegtheta = Qantinegtheta/(NTheta*NTau);
%end

%if corrthetacount > 0
Qcorrnegtheta = Qcorrnegtheta/(NTheta*NTau);
%end

Qnegmintemp = min(Q12(1:NTheta,:));
Qnegmin = min(Qnegmintemp);
Qnegmaxtemp = max(Q12(1:NTheta,:));
Qnegmax = max(Qnegmaxtemp);

%antithetacount = 0;
%corrthetacount = 0;

for p = NTheta+2:2*NTheta+1
    for q = 1:NTau
        if Q12(p,q) < 0
            Qantipostheta = Qantipostheta + Q12(p,q);
            %antithetacount = antithetacount + 1;
        elseif Q12(p,q) > 0
            Qcorrpostheta = Qcorrpostheta + Q12(p,q);
            %corrthetacount = corrthetacount + 1;
        end 
    end
end

%if antithetacount > 0
Qantipostheta = Qantipostheta/(NTheta*NTau);
%end

%if corrthetacount > 0
Qcorrpostheta = Qcorrpostheta/(NTheta*NTau);
%end

Qposmintemp = min(Q12(NTheta+2:2*NTheta+1,:));
Qposmin = min(Qposmintemp);
Qposmaxtemp = max(Q12(NTheta+2:2*NTheta+1,:));
Qposmax = max(Qposmaxtemp);

p = NTheta+1;
for q = 1:NTau
    if Q12(p,q) < 0
        Qantizerotheta = Qantizerotheta + Q12(p,q);
        %antithetacount = antithetacount + 1;
    elseif Q12(p,q) > 0
        Qcorrzerotheta = Qcorrzerotheta + Q12(p,q);
        %corrthetacount = corrthetacount + 1;
    end 
end

%if antithetacount > 0
Qantizerotheta = Qantizerotheta/NTau;
%end

%if corrthetacount > 0
Qcorrzerotheta = Qcorrzerotheta/NTau;
%end

Qzeromin = min(Q12(NTheta+1,:));
Qzeromax = max(Q12(NTheta+1,:));


Qanticustomtheta = 0;
Qcorrcustomtheta = 0;

for p = (NTheta + 1 + nTheta2D-nThetaHalfWidth):(NTheta + 1 + nTheta2D+nThetaHalfWidth)
    for q = 1:NTau
        if Q12(p,q) < 0
            Qanticustomtheta = Qanticustomtheta + Q12(p,q);
            %antithetacount = antithetacount + 1;
        elseif Q12(p,q) > 0
            Qcorrcustomtheta = Qcorrcustomtheta + Q12(p,q);
            %corrthetacount = corrthetacount + 1;
        end 
    end
end

%if antithetacount > 0
Qanticustomtheta = Qanticustomtheta/((2*nThetaHalfWidth+1)*NTau);
%end

%if corrthetacount > 0
Qcorrcustomtheta = Qcorrcustomtheta/((2*nThetaHalfWidth+1)*NTau);
%end

Qcustommintemp = min(Q12((NTheta + 1 + nTheta2D-nThetaHalfWidth):(NTheta + 1 + nTheta2D+nThetaHalfWidth),:));
Qcustommin = min(Qcustommintemp);
Qcustommaxtemp = max(Q12((NTheta + 1 + nTheta2D-nThetaHalfWidth):(NTheta + 1 + nTheta2D+nThetaHalfWidth),:));
Qcustommax = max(Qcustommaxtemp);

IntParams.Qantinegtheta = Qantinegtheta;
IntParams.Qnegmin = Qnegmin;
IntParams.Qcorrnegtheta = Qcorrnegtheta;
IntParams.Qnegmax = Qnegmax;
IntParams.Qantipostheta = Qantipostheta;
IntParams.Qposmin = Qposmin;
IntParams.Qposmax = Qposmax;
IntParams.Qantizerotheta = Qantizerotheta;
IntParams.Qzeromin = Qzeromin;
IntParams.Qcorrzerotheta = Qcorrzerotheta;
IntParams.Qzeromax = Qzeromax;
IntParams.nTheta2D = nTheta2D;
IntParams.nThetaHalfWidth = nThetaHalfWidth;
IntParams.Qanticustomtheta = Qanticustomtheta;
IntParams.Qcustommin = Qcustommin;
IntParams.Qcorrcustomtheta = Qcorrcustomtheta;
IntParams.Qcustommax = Qcustommax;

sep = ',';
stringtemp = [num2str(TStart) sep num2str(Qantinegtheta) sep num2str(Qnegmin) sep num2str(Qcorrnegtheta) sep num2str(Qnegmax) ...
    sep num2str(Qantipostheta) sep num2str(Qposmin) sep num2str(Qcorrpostheta) sep num2str(Qposmax)  ...
    sep num2str(Qantizerotheta) sep num2str(Qzeromin) sep num2str(Qcorrzerotheta) sep num2str(Qzeromax) ...
    sep num2str(nTheta2D) sep num2str(nThetaHalfWidth) sep num2str(Qanticustomtheta) sep num2str(Qcustommin) sep num2str(Qcorrcustomtheta) sep num2str(Qcustommax)]

fid2 = fopen('doublecorrelator.csv', 'a');
fprintf(fid2,stringtemp);
fprintf(fid2,'\n');
fclose(fid2);

n=-NTheta*DeltaT:DeltaT:NTheta*DeltaT;
m=DeltaT:DeltaT:NTau*DeltaT;
