% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function [m,Q12] = doublecorrelator_tau(T,TauMax,TStart,DeltaT,M1,M2,nGap,nPercentGap,nNorm,nTheta2D);
%----------------------------DOUBLECORRELATOR------------------------------
%doublecorrelator calculates double correlators
%T - averaging interval;
%ThetaMax - theta range;
%TauMax - tau range;
%Tstart - start time;
%DeltaT - step size;
%M1 - name of the first file
%M2 - name of the first file
%--------------------------------------------------------------------------

%Loading the data of the first file into a matrix
V1=dlmread(M1,'\t',0,0);
V2=dlmread(M2,'\t',0,0);

%gets each DeltaT records
if DeltaT > 1
   Vtmp1 = V1(1:DeltaT:size(V1,1),:); 
   clear V1;
   V1 = Vtmp1;
   clear Vtmp1;
   Vtmp2 = V2(1:DeltaT:size(V2,1),:); 
   clear V2;
   V2 = Vtmp2;
   clear Vtmp2;
end

N = floor(T/DeltaT);
NTau = floor(TauMax/DeltaT);
NStart = floor(TStart/DeltaT);
NTheta2D = floor(nTheta2D/DeltaT);
NMin = 1+NStart;
Q12=zeros(NTau,1);

if nGap == -1

    for nTau = 1:NTau
            sigm1 =  sqrt( sum( (V1(NMin:(N-nTau+NStart),2)-V1((NMin+nTau):(N+NStart),2)).^2 ) );
            sigm2 =  sqrt( sum( (V2(NMin:(N-nTau+NStart),2)-V2((NMin+nTau):(N+NStart),2)).^2 ) ); 

            if isequal(sigm1,0) || isequal(sigm2,0)
                Q12(nTau) = 0;     
            else
                nTheta = NTheta2D;
                if nTheta < 0 
                    nMin = 1+abs(nTheta)+NStart;
                    nMax = N-nTau+NStart;
                else
                    nMin = 1+NStart;
                    nMax = N-nTau-abs(nTheta)+NStart;
                end
                if nNorm == 1
                    sigm1 =  sqrt( sum( (V1(nMin:nMax,2)-V1((nMin+nTau):(nMax+nTau),2)).^2 ) );
                    sigm2 =  sqrt( sum( (V2((nMin+nTheta):(nMax+nTheta),2)-V2((nMin+nTau+nTheta):(nMax+nTau+nTheta),2)).^2 ) ); 
                end
                correl = sum( (V1(nMin:nMax,2)-V1((nMin+nTau):(nMax+nTau),2)).*...
                    (V2((nMin+nTheta):(nMax+nTheta),2)-V2((nMin+nTau+nTheta):(nMax+nTau+nTheta),2)) );
                    
                if nNorm == 1
                    if isequal(sigm1,0) || isequal(sigm2,0)
                        Q12(nTau) = 0   
                    else
                        Q12(nTau) = correl/(sigm1.*sigm2);
                    end
                else
                    Q12(nTau) = (N-nTau)/(N-nTau-abs(nTheta))*correl/(sigm1.*sigm2);   
                end
                
            end
    end 
      
end

m=DeltaT:DeltaT:NTau*DeltaT;
