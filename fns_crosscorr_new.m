% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function [TAU,THETA,Q12] = fns_crosscorr_new(V1,V2,T,rangeTau,rangeTheta,dt,Gap,nPercentGap),
%**************************************************************************
%fns_crosscorr_new - ��������� �������� �������������� ����� �� ������
%��������� � ������ ��� ������ ������������� �����������.
%V1 � V2 - ��������� ���� ����� �������� ����������� �������������� �����.
%���� ������ ���� ��������� �� ��������� � �������� ����� � ��������� �
%���������� ����������� ������������.
%T - ��������� ��������, �� �������� ����������� �������������� �����.
%rangeTau - ��������, ��������������� "������� ��������".
%rangeTheta - ��������, ��������������� "�������� �� �������".
%���� ��������� rangeTau � rangeTheta ������ ����������� � ��������
%���������� ��������� � (��������, rangeTau = 1:100, rangeTheta = -50:50),
%�� ������� ���������� ������ Q12 ������������
%[length(rangeTau),length(rangeTheta)], ������ TAU ������������
%length(rangeTau) � ������ THETA ������������ length(rangeTheta). ���� ����
%�� ���������� ����� ������������� ���������, � ������ ����������
%(��������, rangeTau = 100, rangeTheta = -50:50), �� ������� ����������
%������ Q12 ������������ [1, length(rangeTheta)], �������������� ����� ����
%����������� ���������������� ����� ��� THETA ��� TAU = 100. �����������
%������� ����������� ���� ����������� ����������� ����� ��� TAU ���
%������������ �������� ��������� rangeTheta. ���� ��� ��������� �����������
%������� ���������� ������ Q12, ������ ������������ ���������� ��
%����������� ���������� TAU � THETA. �������� ����������� ���������
%rangeTau � rangeTheta ����� ��������� ��������� �������� �����������
%����������������. 
%dt - �������� ������������� ��������� �����.
%Gap - ��� ���������. �������� Gap = 0 ������������� ���������� ���������.
%nPercentGap - ���������� ������� ���������.
%**************************************************************************

NT=floor(T/dt);
rTau=floor(min(rangeTau)/dt:max(rangeTau)/dt);
rTheta=floor(min(rangeTheta)/dt:max(rangeTheta)/dt);
Q12=zeros(length(rTheta),length(rTau));
if Gap == 0,
   for i = 1:length(rTau),
       for j = 1:length(rTheta),
            if rTheta(j) < 0
                Tmin = 1+abs(rTheta(j));
                Tmax = NT-rTau(i);
            else
                Tmin = 1;
                Tmax = NT-rTau(i)-abs(rTheta(j));
            end
            sigm1 =  sqrt( sum( (V1(Tmin:Tmax)-V1((Tmin+rTau(i)):(Tmax+rTau(i)))).^2 ) );
            sigm2 =  sqrt( sum( (V2((Tmin+rTheta(j)):(Tmax+rTheta(j)))-...
                V2((Tmin+rTau(i)+rTheta(j)):(Tmax+rTau(i)+rTheta(j)))).^2 ) );
            
            correl = sum( (V1(Tmin:Tmax)-V1((Tmin+rTau(i)):(Tmax+rTau(i)))).*...
                (V2((Tmin+rTheta(j)):(Tmax+rTheta(j)))-V2((Tmin+rTau(i)+rTheta(j)):(Tmax+rTau(i)+rTheta(j)))) );
            
            if isequal(sigm1,0) || isequal(sigm2,0)
                Q12(j,i) = 0;
            else
                Q12(j,i) = correl/(sigm1.*sigm2);
            end
       end
   end
else
    for i = 1:length(rTau),
       for j = 1:length(rTheta),
            if rTheta(j) < 0
                Tmin = 1+abs(rTheta(j));
                Tmax = NT-rTau(i);
            else
                Tmin = 1;
                Tmax = NT-rTau(i)-abs(rTheta(j));
            end
            sigm1 = 0;
            sigm2 = 0;
            correl = 0;
            k = 0;
            for n = 0:(Tmax - Tmin),
                if (V1(n+Tmin) == Gap) | (V1(n+Tmin+rTau(i)) == Gap) |...
                        (V2(n+Tmin+rTheta(j)) == Gap) | (V2(n+Tmin+rTau(i)+rTheta(j)) == Gap);
                    k = k + 1;
                else
                    sigm1 = sigm1+(V1(n+Tmin)-V1(n+Tmin+rTau(i))).^2;
                    sigm2 = sigm2+(V2(n+Tmin+rTheta(j))-V2(n+Tmin+rTheta(j)+rTau(i))).^2;
                    correl = correl+(V1(n+Tmin)-V1(n+Tmin+rTau(i))).*(V2(n+Tmin+rTheta(j))-V2(n+Tmin+rTheta(j)+rTau(i)));
                end
            end
            if (k > nPercentGap./100.*(NT-rTau(i)-abs(rTheta(j))));
                Q12(j,i) = 0;
            elseif isequal(sigm1,0) || isequal(sigm2,0)
                Q12(j,i) = 0;
            else
                Q12(j,i) = correl/(sqrt(sigm1).*sqrt(sigm2));
            end
       end
    end
end
    
TAU=rTau*dt;
THETA=rTheta*dt;