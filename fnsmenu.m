% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function varargout = fnsmenu(varargin)
% FNSMENU M-file for fnsmenu.fig
%      FNSMENU, by itself, creates a new FNSMENU or raises the existing
%      singleton*.
%
%      H = FNSMENU returns the handle to a new FNSMENU or the handle to
%      the existing singleton*.
%
%      FNSMENU('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FNSMENU.M with the given input arguments.
%
%      FNSMENU('Property','Value',...) creates a new FNSMENU or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before fnsmenu_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to fnsmenu_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help fnsmenu

% Last Modified by GUIDE v2.5 18-May-2012 22:28:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @fnsmenu_OpeningFcn, ...
                   'gui_OutputFcn',  @fnsmenu_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before fnsmenu is made visible.
function fnsmenu_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to fnsmenu (see VARARGIN)

% Choose default command line output for fnsmenu
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes fnsmenu wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = fnsmenu_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function FNS_menu_Callback(hObject, eventdata, handles)
% hObject    handle to FNS_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in btnDifferenceMoment.
function btnDifferenceMoment_Callback(hObject, eventdata, handles)
% hObject    handle to btnDifferenceMoment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_diffmoment


% --- Executes on button press in btnDoubleCorrelator.
function btnDoubleCorrelator_Callback(hObject, eventdata, handles)
% hObject    handle to btnDoubleCorrelator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_doublecorrelator

% --- Executes on button press in btnPowerSpectrum.
function btnPowerSpectrum_Callback(hObject, eventdata, handles)
% hObject    handle to btnPowerSpectrum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_powerspectrum

% --- Executes on button press in btnNonstationarityAdvanced.
function btnNonstationarity_Callback(hObject, eventdata, handles)
% hObject    handle to btnNonstationarityAdvanced (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_nonstationarity

% --- Executes on button press in btnSmoothing.
function btnSmoothing_Callback(hObject, eventdata, handles)
% hObject    handle to btnSmoothing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_smoothing

% --- Executes on button press in btnMusic.
function btnMusic_Callback(hObject, eventdata, handles)
% hObject    handle to btnMusic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_music

% --- Executes on button press in btnSlice.
function btnSlice_Callback(hObject, eventdata, handles)
% hObject    handle to btnSlice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_slicing


% --- Executes on button press in btnTrendRemove.
function btnTrendRemove_Callback(hObject, eventdata, handles)
% hObject    handle to btnTrendRemove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_detrend


% --- Executes on button press in btnMultiplier.
function btnMultiplier_Callback(hObject, eventdata, handles)
% hObject    handle to btnMultiplier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_multiplier


% --- Executes on button press in btnDisplaySeries.
function btnDisplaySeries_Callback(hObject, eventdata, handles)
% hObject    handle to btnDisplaySeries (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_display_series


% --- Executes on button press in btnFullParamFinal.
function btnFullParamFinal_Callback(hObject, eventdata, handles)
% hObject    handle to btnFullParamFinal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_general_final



% --- Executes on button press in btnDiscretization.
function btnDiscretization_Callback(hObject, eventdata, handles)
% hObject    handle to btnDiscretization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_discretization



% --- Executes on button press in btnKaplanEEG.
function btnKaplanEEG_Callback(hObject, eventdata, handles)
% hObject    handle to btnKaplanEEG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_kaplan_EEG



% --- Executes on button press in param_v2.
function param_v2_Callback(hObject, eventdata, handles)
% hObject    handle to param_v2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_general_final_v2



% --- Executes on button press in btnchaoticparam.
function btnchaoticparam_Callback(hObject, eventdata, handles)
% hObject    handle to btnchaoticparam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_chaotic



% --- Executes on button press in param_v3.
function param_v3_Callback(hObject, eventdata, handles)
% hObject    handle to param_v3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_general_final_v3



% --- Executes on button press in param_v4.
function param_v4_Callback(hObject, eventdata, handles)
% hObject    handle to param_v4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_general_final_v4



% --- Executes on button press in addgaps.
function addgaps_Callback(hObject, eventdata, handles)
% hObject    handle to addgaps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_addgaps



% --- Executes on button press in removegaps.
function removegaps_Callback(hObject, eventdata, handles)
% hObject    handle to removegaps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_remove_gaps



% --- Executes on button press in removeduplicates.
function removeduplicates_Callback(hObject, eventdata, handles)
% hObject    handle to removeduplicates (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_remove_duplicates



% --- Executes on button press in finduplicates.
function finduplicates_Callback(hObject, eventdata, handles)
% hObject    handle to finduplicates (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_find_duplicates



% --- Executes on button press in param_v5.
function param_v5_Callback(hObject, eventdata, handles)
% hObject    handle to param_v5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_general_final_v5


% --- Executes on button press in btnPhysRevEParam.
function btnPhysRevEParam_Callback(hObject, eventdata, handles)
% hObject    handle to btnPhysRevEParam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_general_physreve



% --- Executes on button press in btnAstroPhysParam.
function btnAstroPhysParam_Callback(hObject, eventdata, handles)
% hObject    handle to btnAstroPhysParam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_general_astrophys



% --- Executes on button press in btnAstroPhysDeTrend.
function btnAstroPhysDeTrend_Callback(hObject, eventdata, handles)
% hObject    handle to btnAstroPhysDeTrend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_detrend_AP



% --- Executes on button press in btnShowGaps.
function btnShowGaps_Callback(hObject, eventdata, handles)
% hObject    handle to btnShowGaps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_show_gaps

% --- Executes on button press in btnMultipleDoubleCorrelator.
function btnMultipleDoubleCorrelator_Callback(hObject, eventdata, handles)
% hObject    handle to btnMultipleDoubleCorrelator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_multiple_doublecorrelator

% --- Executes on button press in btnpsdfastrophys.
function btnpsdfastrophys_Callback(hObject, eventdata, handles)
% hObject    handle to btnpsdfastrophys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_ps_df_astrophys

% --- Executes on button press in btnNonstationarityOld.
function btnNonstationarityOld_Callback(hObject, eventdata, handles)
% hObject    handle to btnNonstationarityOld (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_nonstationarity_old

% --- Executes on button press in btnPhysRevEParamOld.
function btnPhysRevEParamOld_Callback(hObject, eventdata, handles)
% hObject    handle to btnPhysRevEParamOld (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_general_physreve_old

% --- Executes on button press in btnPhysRevEParamv1.
function btnPhysRevEParamv1_Callback(hObject, eventdata, handles)
% hObject    handle to btnPhysRevEParamv1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_general_physreve_v1


% --- Executes on button press in btnPhysRevEParamv2.
function btnPhysRevEParamv2_Callback(hObject, eventdata, handles)
% hObject    handle to btnPhysRevEParamv2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_params_general_physreve_v2

% --- Executes on button press in btnSlicingPhysio.
function btnSlicingPhysio_Callback(hObject, eventdata, handles)
% hObject    handle to btnSlicingPhysio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_slicing_physiobank

% --- Executes on key press with focus on btnNonstationarity and none of its controls.
function btnNonstationarity_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to btnNonstationarity (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on button press in btnDCBAP.
function btnDCBAP_Callback(hObject, eventdata, handles)
% hObject    handle to btnDCBAP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_doublecorrelator_BAP


% --- Executes on button press in btnMultipleDCBAP.
function btnMultipleDCBAP_Callback(hObject, eventdata, handles)
% hObject    handle to btnMultipleDCBAP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_multiple_doublecorrelator_BAP


% --- Executes on button press in btnBAPconvert.
function btnBAPconvert_Callback(hObject, eventdata, handles)
% hObject    handle to btnBAPconvert (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_BAP_convert


% --- Executes on button press in btnDCIntegral.
function btnDCIntegral_Callback(hObject, eventdata, handles)
% hObject    handle to btnDCIntegral (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_doublecorrelator_integral


% --- Executes on button press in interpolategaps.
function interpolategaps_Callback(hObject, eventdata, handles)
% hObject    handle to interpolategaps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_interpolate_gaps


% --- Executes on button press in pushbutton77.
function pushbutton77_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton77 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_local_averaging


% --- Executes on button press in btnDoubleCorrelatorTau.
function btnDoubleCorrelatorTau_Callback(hObject, eventdata, handles)
% hObject    handle to btnDoubleCorrelatorTau (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_doublecorrelator_tau

% --- Executes on button press in btnRunCoh.
function btnRunCoh_Callback(hObject, eventdata, handles)
% hObject    handle to btnRunCoh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_coh


% --- Executes on button press in btnDoubleCorrelatorTheta.
function btnDoubleCorrelatorTheta_Callback(hObject, eventdata, handles)
% hObject    handle to btnDoubleCorrelatorTheta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run_doublecorrelator_theta
