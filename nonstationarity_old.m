% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function [m, NF] = nonstationarity_old(T,TEnd,alpha,subinterval,TStart,DeltaT,Ord,type,M1,nGap,nPercentGap);

%----------------------------NONSTATIONARITY-------------------------------
%T - averaging interval;
%TEnd - end of calculation;
%alpha - the value of alpha - range where tau is evaluated;
%subinterval - sliding interval;
%Tstart - start time;
%DeltaT - step size;
%Ord - difference moment order
%type - type of difference moment
%    'cumulant'
%    'regular'
%M1 - file name and path;
%--------------------------------------------------------------------------

%Loading the data of the first file into a matrix
V=dlmread(M1,'\t',0,0);

%gets each DeltaT records
if DeltaT > 1
   Vtmp = V(1:DeltaT:size(V,1),:); 
   clear V;
   V = Vtmp;
   clear Vtmp;
end

N = floor(T/DeltaT);
NEnd = floor((TEnd-T)/subinterval);
NTau = floor(alpha*T/DeltaT);
NStart = floor(TStart/DeltaT);
NSubint = floor(subinterval/DeltaT);
NF=zeros(1,(NEnd-1));

for k = 0:(NEnd-2)   
   Q2 = sum( diffmomentSub(V((1+(k+1)*NSubint+NStart):(N+(k+1)*NSubint+NStart),2),N,NTau,Ord,type,nGap, nPercentGap) );   
   Q1 = sum( diffmomentSub(V((1+k*NSubint+NStart):(N+k*NSubint+NStart),2),N,NTau,Ord,type,nGap, nPercentGap) );  
   if isequal(Q2+Q1,0)
       NF(k+1) = 0;
   else
       NF(k+1) = 2*N/NSubint*(Q2-Q1)/(Q2+Q1);
   end
end

mrange = (1+NSubint+N+NStart):NSubint:(1+(NEnd-1)*NSubint+N+NStart);
m=V(mrange,1)';

    
