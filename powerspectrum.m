% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function [m,PS] = powerspectrum(T,TStart,DeltaT,nSkip,nTotal,nNorma,nSize,nOrder,nType,M1,nGap,nPercentGap);

%----------------------------POWERSPECTRUM---------------------------------
%T - averaging interval;
%Tstart - start time;
%DeltaT - step size;
%M1 - filename and path;
%--------------------------------------------------------------------------

%Loading the data of the first file into a matrix
V=dlmread(M1,'\t',0,0);

%gets each DeltaT records
if DeltaT > 1
   Vtmp = V(1:DeltaT:size(V,1),:); 
   clear V;
   V = Vtmp;
   clear Vtmp;
end

N = floor(T/DeltaT);
NStart = floor(TStart/DeltaT);
NTotal = floor(nTotal/DeltaT);
NMin = 1+NStart;

switch nType
    case 0,
        %mean1 = mean(V(NMin:(N+NStart),2)).^2;
        pN = NTotal;
        
        if nGap == -1
            Tmp1 = xcorr(V(NMin:(N+NStart),2),pN,'unbiased');
        else            
            Tmp1=zeros(2*pN+1,1);
            for nTau = 0:pN
                Tmp1(nTau+1) = 0;
                i = 0;
                for n = 1:(N-nTau)                   
                    if (V(n+NStart,2) == nGap) | (V(n+nTau+NStart,2) == nGap)
                        i = i + 1;
                    else
                        Tmp1(nTau+1) = Tmp1(nTau+1) + V(n+NStart,2).*V(n+nTau+NStart,2);
                    end  
                end
                if i > nPercentGap./100.*(N-nTau)  
                    Tmp1(nTau+1) = 0;
                else
                    Tmp1(nTau+1) = 1/(N-nTau-i)*Tmp1(nTau+1);                       
                end                             
            end
            Tmp1((pN+1):(2*pN+1))=Tmp1(1:pN+1);
        end
                
        Vcor = zeros(2*pN,1);
        Vcor(1:(pN+1)) = Tmp1((pN+1):(2*pN+1));
        Vcor((pN+2):(2*pN)) = Vcor(pN:-1:2);
        %PS = fft(Vcor - mean1);    
        PS = fft(Vcor);    
        PS = PS(1:(floor(length(PS)/2)+1));
        PS = real(PS);
        PS = PS';
        if nNorma == 1
            PS = 1/length(PS)*PS;
        end
        PS(2:length(PS)-1) = 2*PS(2:length(PS)-1);
        m = 0:(length(PS)-1);        
        m = m./(2*pN);            
    case 1,
        [PS,m] = periodogram(V((1+NStart):(N+NStart),2),[],N,[]);
        PS = PS';
        m = m';
    case 2,
        [PS,m] = pwelch(V((1+NStart):(N+NStart),2),[],[],floor(N/4.5),[]);
        PS = PS';
        m = m';        
    case 3,
        [PS,m] = pburg(V((1+NStart):(N+NStart),2),nOrder,nSize,[]);
        PS = PS';
        m = m';          
    case 4,
        [PS,m] = pyulear(V((1+NStart):(N+NStart),2),nOrder,nSize,[]);
        PS = PS';
        m = m';     
    case 5,
        %mean1 = mean(V(NMin:(N+NStart),2)).^2;
        pN = NTotal;
        Tmp1 = xcorr(V(NMin:(N+NStart),2),pN,'unbiased');
        Vcor = zeros(pN+1,1);
        Vcor(1:(pN+1)) = Tmp1((pN+1):(2*pN+1));                                  
        PS1 = zeros(pN+1,1);
        Vcor1 = Vcor;%-mean1;                
        for p=0:pN
            nsum = 0;
            for n=2:pN
                nsum = nsum + Vcor1(n).*cos(pi*p*(n-1)./pN);
            end
            PS1(p+1) = Vcor1(1)+Vcor1(pN+1).*(-1).^p + 2.*nsum;
        end                                                 
        PS = PS1;%abs(PS1);
        PS(2:length(PS)-1) = 2*PS(2:length(PS)-1);
        PS = PS';                       
        m = 0:(length(PS)-1);        
        m = m./(2*pN);              
    %case 6,
    %    PS = 1/N*abs(fft(V((1+NStart):(N+NStart),2))).^2;
    %    PS = PS(1:(floor(length(PS)/2)+1));
    %    PS(2:length(PS)-1) = 2*PS(2:length(PS)-1);
    %    PS = PS';
    %    m = 0:(length(PS)-1);             
    %    m = m./N;           
end

N1 = length(PS);
m = m((nSkip+1):N1);
PS = PS((nSkip+1):N1);

   
    %{
        case 1,
        PS = 1/N*abs(fft(V((1+NStart):(N+NStart),2))).^2;
        PS = PS(1:(floor(length(PS)/2)+1));
        PS(2:length(PS)-1) = 2*PS(2:length(PS)-1);
        PS = PS';
        m = 0:(length(PS)-1);             
        m = m./N;   
    %}
            %mean1 = (1/N*sum(V(NMin:(N+NStart),2)))                                               
     %}       