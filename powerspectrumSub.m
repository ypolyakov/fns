% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function [m,PS] = powerspectrumSub(V,T,TStart,DeltaT,nSkip,nTotal,nNorma);

%----------------------------POWERSPECTRUMSUB---------------------------------
%T - averaging interval;
%Tstart - start time;
%DeltaT - step size;
%M1 - filename and path;
%--------------------------------------------------------------------------

%gets each DeltaT records
if DeltaT > 1
   Vtmp = V(1:DeltaT:length(V),:); 
   clear V;
   V = Vtmp;
   clear Vtmp;
end

N = floor(T/DeltaT);
NStart = floor(TStart/DeltaT);
NTotal = floor(nTotal/DeltaT);
NMin = 1+NStart;

%mean1 = mean(V(NMin:(N+NStart))).^2;
pN = NTotal;
Tmp1 = xcorr(V(NMin:(N+NStart)),pN,'unbiased');
Vcor = zeros(2*pN,1);
Vcor(1:(pN+1)) = Tmp1((pN+1):(2*pN+1));
Vcor((pN+2):(2*pN)) = Vcor(pN:-1:2);    
%PS = fft(Vcor - mean1); 
PS = fft(Vcor); 
PS = PS(1:(floor(length(PS)/2)+1));
PS = real(PS);
PS = PS';
PS(2:length(PS)-1) = 2*PS(2:length(PS)-1);
if nNorma == 1
    PS = 1/length(PS)*PS;
end
m = 0:(length(PS)-1);        
m = m./(2*pN);            
    
N1 = length(PS);
m = m((nSkip+1):N1);
PS = PS((nSkip+1):N1);