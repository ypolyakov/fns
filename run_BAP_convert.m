% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_BAT_convert()

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Specify the input directory');

list = dir(PathName1);

for iter = 3:length(list)   
    
    M1=[PathName1,list(iter).name];
    
    fid = fopen(M1);
    V = textscan(fid, '%s %s');
    fclose(fid);
    
    V2 = zeros(length(V{1}),2);
    
    for iter2 = 1:length(V{1})
        V2(iter2,1) = str2double(strrep(V{1}(iter2),',','.'));
        V2(iter2,2) = str2double(strrep(V{2}(iter2),',','.'));        
    end     
    
    FileTemp = [M1(1:length(M1)-4) '-FNS.txt'];
    dlmwrite([FileTemp],[V2(:,1),V2(:,2)], 'delimiter','\t', 'precision', 15); 
    ['Created ',FileTemp]
    
end

%for iter = 2:length(V2{2})
%    FileTemp = [FileName1(1:length(FileName1)-4) '-' strtrim(cell2mat(V2{2}(iter))) '.txt'];
%   dlmwrite([PathName1,'\',FileTemp],[V1(:,1),V1(:,iter)], 'delimiter','\t', 'precision', 15);    
%end




