% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_addgaps()
% Prompts for the parameters
answer = inputdlg({['Step size:'] ['Gap value:']},...
    'Add blank values for gaps',1,...
    {['0.55'] ['-999.9'] });

result = str2double(answer);

dStep = result(1);
dGap = result(2);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Specify the original series');
M1=[PathName1,FileName1]; 

%Loading the data of the first file into a matrix
V=dlmread(M1,'\t',0,0);

n = (V(length(V),1) - V(1,1))./dStep + 1;

V2 = zeros(n,2);

k = 1;

for i = 1:n
    cur = (i-1).*dStep + V(1,1);
    V2(i,1) = cur;
    if cur < V(k,1)        
        V2(i,2) = dGap;
    else
        V2(i,2) = V(k,2);
        k = k + 1;
    end
end


[FileName2,PathName2]=uiputfile('*.txt','Specify the output file',FileName1);
dlmwrite([PathName2,'\',FileName2],[V2],'delimiter','\t', 'precision', 15);



