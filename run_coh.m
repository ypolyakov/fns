% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Prompts for the parameters
answer = inputdlg({['Averaging interval (T):'] ['End value for tau (in units of T):']...
    ['Start point index:'] ['Step size:'] ...
    ['Coherence (0 - PD, 1 - CC, 2 - DC):'] ...
    ['Window type (0 - rect, 1 - hamming):'] ...
    ['Value of theta (in points):']},...
    'Parameters for coherence',1,...
    {['4096'] ['0.25'] ['0'] ['1'] ['2'] ['0'] ['0'] });

result = str2double(answer);

T = result(1);
TauMax = floor(result(2)*T);
TStart = result(3);
DeltaT = result(4);
cohtype = result(5);
wintype = result(6);
Theta2D = floor(result(7));

%Prompts for the first file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the first file');
M1=[PathName1,FileName1]; 
%Prompts for the second file name
[FileName2,PathName2] = uigetfile('*.txt','Choose the second file');
M2=[PathName2,FileName2]; 

switch cohtype
    case 0
        [m,Q12] = coh_fft_tau(T,TStart,DeltaT,M1,M2,Theta2D,wintype);
    case 1
        [m,Q12] = coh_cc_tau(T,TauMax,TStart,DeltaT,M1,M2,Theta2D,wintype);
    case 2
        [m,Q12] = coh_dc_tau(T,TauMax,TStart,DeltaT,M1,M2,Theta2D,wintype);        
end

h2 = figure();
plot(m,Q12(:));
title({['\theta = ',num2str(Theta2D), ', T = ',num2str(T),', tstart = ',num2str(TStart),...
    ', \Deltat = ',num2str(DeltaT), ', FileName1: ',num2str(FileName1),...
    ', FileName2: ',num2str(FileName2)]});
xlim([min(m) max(m)]);
%ylim([0 1]);
xlabel('f','fontsize',16);
ylabel('coherence','fontsize',14);


%Save the results using the tab-delimited format
button = questdlg('Do you want to save the results?','Results','Yes');
if isequal(button,'Yes')
   [FileName2,PathName2]=uiputfile('*.txt','Specify the output file for constant theta');
   dlmwrite([PathName2,'\',FileName2],[m', Q12],'delimiter','\t', 'precision', 15); 
end