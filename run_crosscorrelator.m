% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Prompts for the parameters
answer = inputdlg({['Averaging interval (T):']...
    ['End value for theta (in units of T):'] ['Start point index:']},...  
    'Parameters for double correlator',1,...
    {['400'] ['0.2'] ['0']});

result = str2double(answer);

T = result(1);
ThetaMax = floor(result(2)*T);
TStart = result(3);

%Prompts for the first file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the first file');
M1=[PathName1,FileName1]; 
%Prompts for the second file name
[FileName2,PathName2] = uigetfile('*.txt','Choose the second file');
M2=[PathName2,FileName2]; 

[n,Q1] = crosscorrelator(T,ThetaMax,TStart,M1,M2);

h2 = figure();
plot(n,Q1);
title({['T = ',num2str(T),', tstart = ',num2str(TStart),...
  ', FileName1: ',num2str(FileName1),...
    ', FileName2: ',num2str(FileName2)]});
xlim([min(n) max(n)]);
%ylim([0 1]);
xlabel('\theta','fontsize',16);
ylabel('correlation','fontsize',14);


%Save the results using the tab-delimited format
button = questdlg('Do you want to save the results?','Results','Yes');
if isequal(button,'Yes')
   [FileName2,PathName2]=uiputfile('*.txt','Specify the output file');
   dlmwrite([PathName2,'\',FileName2],[n', Q1'],'delimiter','\t', 'precision', 15);
end