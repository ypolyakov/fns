% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_detrend_AP()
% Prompts for the parameters
answer = inputdlg({['Step size:'] ['Inactivity value:']...
    ['Type: (0)-mean, (1)-polynomial, (2)-exponential:'] ['Polynomial order:']...
    ['Exp: starting value for A'] ['Exp: starting value for a'] ['Exp: starting value for n']...
    ['Exp: lower value for A'] ['Exp: lower value for a'] ['Exp: lower value for n']},...
    'Parameters for detrending',1,...
    {['0.0625'] ['0'] ['0'] ['1'] ['1'] ['0.5'] ['0.5'] ['0'] ['0'] ['0']});

result = str2double(answer);

dStep = result(1);
dGap = result(2);
dType = result(3);
pOrder = floor(result(4));
expA = result(5);
expa = result(6);
expn = result(7);
lexpA = result(8);
lexpa = result(9);
lexpn = result(10);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Specify the file to detrend');

list = dir(PathName1);

for iter = 3:length(list)

    %calculate the positions for the figures
    bdwidth = 5;
    topbdwidth = 30;
    set(0,'Units','pixels');
    scnsize = get(0,'ScreenSize');

    pos1  = [bdwidth,... 
        1/3*scnsize(4) + bdwidth,...
        scnsize(3)/2 - 2*bdwidth,...
        scnsize(4)/2 - (topbdwidth + bdwidth)];
    pos2 = [pos1(1) + scnsize(3)/2,...
        pos1(2),...
        pos1(3),...
        pos1(4)];

    M1=[PathName1,list(iter).name];
    
    %Loading the data of the first file into a matrix
    V=dlmread(M1,'\t',0,0);
    
    ind = find(V(:,2)==dGap);
    
    if length(ind) > 0
        bGapped = 1;
    else
        bGapped = 0;
    end
        
    V(ind,:) = [];

    switch dType
        case 0,
            %h1 = figure('Position',pos1);
            mean1 = mean(V(:,2));
            %Vt = mean1.*ones(length(V),1);
            %plot(V(:,1),V(:,2),V(:,1),Vt);
            %legend('experimental', 'mean');
            V(:,2) = V(:,2) - mean1;
        case 1,
            p = polyfit(V(:,1),V(:,2),pOrder);
            f = polyval(p,V(:,1));
            h1 = figure('Position',pos1);
            plot(V(:,1),V(:,2),V(:,1),f);
            title({['polynomial order = ', num2str(pOrder)]});
            legend('experimental', 'polynomial interpolation');
            V(:,2) = V(:,2) - f;
        case 2,
            vmin = min(V(:,2));
            t0 = V(1,1);
            Vnew = V;
            Vnew(:,1) = V(:,1)-t0;        
            Vnew(:,2) = V(:,2)-vmin;

            newRange = (1:length(Vnew)/100:length(Vnew));
            newRange = unique(floor(newRange));

            VnewSliced = Vnew(newRange,:);        

            % --- Create fit
            stExp = [expA expa expn];
            ftExp = fittype(['A*exp(-a*t^n)'] ,...
             'dependent',{'V'},'independent',{'t'},...
             'coefficients',{'A', 'a', 'n'});

            % Fit this model
            cfExp = fit(VnewSliced(:,1),VnewSliced(:,2),ftExp ,'Startpoint',stExp, 'Lower',[lexpA lexpa lexpn]);

            fittedExp = fExp(Vnew(:,1)',cfExp.A,cfExp.a,cfExp.n)';        

            fittedExp = fittedExp + vmin;

            h1 = figure('Position',pos1);
            plot(V(:,1),V(:,2),V(:,1),fittedExp);
            title({['A = ', num2str(cfExp.A),' a = ',num2str(cfExp.a),' n = ',num2str(cfExp.n)]});
            %legend('experimental', 'polynomial interpolation');
            V(:,2) = V(:,2) - fittedExp;        
    end

    n = (V(length(V),1) - V(1,1))./dStep + 1;

    V2 = zeros(n,2);

    k = 1;

    for i = 1:n
        cur = (i-1).*dStep + V(1,1);
        V2(i,1) = cur;
        if cur < V(k,1)        
            V2(i,2) = dGap;
        else
            V2(i,2) = V(k,2);
            k = k + 1;
        end
    end

    %h2 = figure('Position',pos2);
    %plot(V2(:,1),V2(:,2));
    %title(['detrended signal']);

    %if bGapped == 1
    %    suffix = '-g.txt';
    %else
    %    suffix = '.txt';
    %end
    
    %if bGapped == 1
    %    suffix = '-detr-g.txt';
    %else
    suffix = '-detr.txt';
    %end    
    
    fOutput1 = strrep(M1, '.txt', suffix);
    dlmwrite(fOutput1,[V2],'delimiter','\t', 'precision', 15);

    [strrep(list(iter).name, '.txt', suffix) char(9) num2str(mean1)]
    
end

function fittedExp = fExp(t,A,a,n)
fittedExp = A.*exp(-a.*t.^n);


