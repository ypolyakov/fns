% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Prompts for the parameters
answer = inputdlg({['Averaging interval (T):'] ['End value for tau (in units of T):']...
    ['Start point index:'] ['Step size:'] ['Type, regular(0) or cumulant(1):']...
    ['Order of difference moment']...
    ['Value for missing data (FNS only, -1 - no missing data):']...
    ['Percent of gaps allowed:']},...
    'Parameters for difference moment',1,...
    {['500'] ['0.5'] ['0'] ['1'] ['0'] ['2'] ['-1'] ['25']});

result = str2double(answer);

T = result(1);
TauMax = floor(result(2)*T);
TStart = result(3);
DeltaT = result(4);
type = result(5);
Ord = result(6);
nGap = result(7);
nPercentGap = result(8);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the first file');
M1=[PathName1,FileName1]; 

[m, DF] = diffmoment(T,TauMax,TStart,DeltaT,Ord,type,M1,nGap,nPercentGap);

figure();

plot(m,DF);
xlabel('\tau','fontsize',16);

if isequal(type,0)
    stype = 'regular';
else
    stype = 'cumulant';
end
ylabel(['difference moment: ' stype '; order: '  num2str(Ord)],'fontsize',16);

title(['T = ',num2str(T),', tstart = ',num2str(TStart),...
    ', \Deltat = ',num2str(DeltaT), ', FileName: ',num2str(FileName1)]);

%Save the results using the tab-delimited format
button = questdlg('Do you want to save the results?','Results','Yes');
if isequal(button,'Yes')
   [FileName2,PathName2]=uiputfile('*.txt','Specify the output file');
   dlmwrite([PathName2,'\',FileName2],[m', DF'], 'delimiter','\t', 'precision', 15);
end