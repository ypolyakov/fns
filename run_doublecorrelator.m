% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Prompts for the parameters
answer = inputdlg({['Averaging interval (T):'] ['End value for tau (in units of T):']...
    ['End value for theta (in units of T):'] ['Start point index:'] ['Step size:'] ...
    ['Graph: surf(0), contour(1), 2D surf(2)'] ...
    ['Value for missing data (FNS only, -1 - no missing data):']...
    ['Percent of gaps allowed:']...
    ['Value of theta for 2D projection (in units of T):'],...
    ['Value of tau for 2D projection (in units of T):'],...
    ['Normalized: Yes(1) or No(0)'],...
    ['Halfwidth for theta: ']},...
    'Parameters for double correlator',1,...
    {['400'] ['0.3'] ['0.2'] ['0'] ['1'] ['0'] ['-1'] ['25'] ['0'] ['0.1'] ['1'] ['0.05']});

result = str2double(answer);

T = result(1);
TauMax = floor(result(2)*T);
ThetaMax = floor(result(3)*T);
TStart = result(4);
DeltaT = result(5);
graphtype = result(6);
nGap = result(7);
nPercentGap = result(8);
Theta2D = floor(result(9)*T);
Tau2D = floor(result(10)*T);
nNorm = result(11);
nTauTrace = 1;
nThetaTrace = 0;
HalfWidth = floor(result(12)*T);

%Prompts for the first file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the first file');
M1=[PathName1,FileName1]; 
%Prompts for the second file name
[FileName2,PathName2] = uigetfile('*.txt','Choose the second file');
M2=[PathName2,FileName2]; 

sep = ',';
fid2 = fopen('doublecorrelator.csv', 'a');
stringtemp = ['Tstart' sep 'q--av' sep 'q-min' sep 'q-+av' sep 'q-max' ...
    sep 'q+-av' sep 'q+min' sep 'q++av' sep 'q+max' ...
    sep 'q0-av' sep 'q0min' sep 'q0+av' sep 'q0max' ...
    sep 'theta' sep 'halfwidth' sep 'qc-av' sep 'qcmin' sep 'qc+av' sep 'qcmax']
fprintf(fid2,stringtemp);
fprintf(fid2,'\n');
fclose(fid2);

[m,n,Q12,mTrace] = doublecorrelator(T,TauMax,ThetaMax,TStart,DeltaT,M1,M2,nGap,nPercentGap,nNorm,nTauTrace,nThetaTrace,0,Theta2D,HalfWidth);

%calculate the positions for the figures
bdwidth = 5;
topbdwidth = 30;
set(0,'Units','pixels');
scnsize = get(0,'ScreenSize');

pos3  = [bdwidth,... 
    bdwidth+topbdwidth,...
    scnsize(3)/2 - 2*bdwidth,...
    2*scnsize(4)/5 - (topbdwidth + bdwidth)];
pos1 = [pos3(1),...
    pos3(2) + scnsize(4)/2,...
    pos3(3),...
    pos3(4)];
pos2 = [pos3(1) + scnsize(3)/2,...
    pos3(2) + scnsize(4)/2,...
    pos3(3),...
    pos3(4)];
pos4 = [pos3(1) + scnsize(3)/2,...
    pos3(2),...
    pos3(3),...
    pos3(4)];

h1 = figure('Position',pos1);

%Type of drawing
switch graphtype
    case 2,
        surf(m,n,Q12); shading interp;
        view([0 90])
    case 0,
        surf(m,n,Q12); shading interp;
    case 1,
        contour(m,n,Q12,30);
end
set(gca,'DataAspectRatio',[length(m)/length(n) 1 0.01],'linewidth',1,'CLim',[-1 1]);
title({['T = ',num2str(T),', tstart = ',num2str(TStart),...
    ', \Deltat = ',num2str(DeltaT), ', FileName1: ',num2str(FileName1),...
    ', FileName2: ',num2str(FileName2)]});
xlim([min(m) max(m)]);
ylim([min(n) max(n)]);
if graphtype == 2
    zlim([-1 1]);
else
    zlim([min(min(Q12)) max(max(Q12))]);
end
c=colorbar('vert');
xlabel('\tau','fontsize',16);
ylabel('\theta','fontsize',16);
zlabel('correlation','fontsize',14);

n2D = find(n==Theta2D);

h2 = figure('Position',pos2);
plot(m,Q12(n2D,:));
title({['\theta = ',num2str(Theta2D), ', T = ',num2str(T),', tstart = ',num2str(TStart),...
    ', \Deltat = ',num2str(DeltaT), ', FileName1: ',num2str(FileName1),...
    ', FileName2: ',num2str(FileName2)]});
xlim([min(m) max(m)]);
%ylim([0 1]);
xlabel('\tau','fontsize',16);
ylabel('correlation','fontsize',14);

t2D = find(m==Tau2D);

h3 = figure('Position',pos3);
plot(n,Q12(:,t2D));
title({['\tau = ',num2str(Tau2D), ', T = ',num2str(T),', tstart = ',num2str(TStart),...
    ', \Deltat = ',num2str(DeltaT), ', FileName1: ',num2str(FileName1),...
    ', FileName2: ',num2str(FileName2)]});
xlim([min(n) max(n)]);
%ylim([0 1]);
xlabel('\theta','fontsize',16);
ylabel('correlation','fontsize',14);

[C,I] = max(Q12);

h4 = figure('Position',pos4);
plot(m,n(I));
title({['T = ',num2str(T),', tstart = ',num2str(TStart),...
    ', \Deltat = ',num2str(DeltaT), ', FileName1: ',num2str(FileName1),...
    ', FileName2: ',num2str(FileName2)]});
xlim([min(m) max(m)]);
ylim([unique(min(n(I)))-1 unique(max(n(I)))+1]);
xlabel('\tau','fontsize',16);
ylabel('\theta maximum','fontsize',14);


%h5 = figure('Position',pos4);
%plot(mTrace(:,1),mTrace(:,2));
%title({['T = ',num2str(T),', tstart = ',num2str(TStart),...
%    ', TauTrace = ',num2str(nTauTrace),', ThetaTrace = ',num2str(nThetaTrace)];
%    ['\Deltat = ',num2str(DeltaT), ', FileName1: ',num2str(FileName1),...
%    ', FileName2: ',num2str(FileName2)]});
%xlim([min(m) max(m)]);
%ylim([unique(min(n(I)))-1 unique(max(n(I)))+1]);
%xlabel('t (points)','fontsize',14);
%ylabel('double correlator fraction','fontsize',14);

%Save the results using the tab-delimited format
button = questdlg('Do you want to save the results?','Results','Yes');
if isequal(button,'Yes')
   [FileName2,PathName2]=uiputfile('*.txt','Specify the output file for constant theta');
   dlmwrite([PathName2,'\',FileName2],[m', Q12(n2D,:)'],'delimiter','\t', 'precision', 15);
   [FileName3,PathName3]=uiputfile('*.txt','Specify the output file for constant tau');
   dlmwrite([PathName3,'\',FileName3],[n', Q12(:,t2D)],'delimiter','\t', 'precision', 15);   
end