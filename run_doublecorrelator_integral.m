% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Prompts for the parameters
answer = inputdlg({['Averaging interval (T):'] ['End value for tau (in units of T):']...
    ['End value for theta (in units of T):'] ['Start point index:'] ['End point index:'] ...
    ['Value for missing data (FNS only, -1 - no missing data):']...
    ['Percent of gaps allowed:']...
    ['Halfwidth for theta (in units of T): ']},...
    'Parameters for double correlator',1,...
    {['400'] ['0.3'] ['0.2'] ['0'] ['10'] ['-1'] ['25'] ['0.1']});

result = str2double(answer);

T = result(1);
TauMax = floor(result(2)*T);
ThetaMax = floor(result(3)*T);
TStart = result(4);
TEnd = result(5);
nGap = result(6);
nPercentGap = result(7);
HalfWidth = floor(result(8)*T);

%Prompts for the first file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the first file');
M1=[PathName1,FileName1]; 
%Prompts for the second file name
[FileName2,PathName2] = uigetfile('*.txt','Choose the second file');
M2=[PathName2,FileName2]; 

sep = ',';
fid2 = fopen('doublecorrelator.csv', 'a');
stringtemp = ['Tstart' sep 'q--av' sep 'q-min' sep 'q-+av' sep 'q-max' ...
    sep 'q+-av' sep 'q+min' sep 'q++av' sep 'q+max' ...
    sep 'q0-av' sep 'q0min' sep 'q0+av' sep 'q0max' ...
    sep 'theta' sep 'halfwidth' sep 'qc-av' sep 'qcmin' sep 'qc+av' sep 'qcmax']
fprintf(fid2,stringtemp);
fprintf(fid2,'\n');
fclose(fid2);

ccint=zeros(TEnd-TStart+1,2);

for i = TStart:TEnd

    [m,n,Q12,mTrace,IntParams] = doublecorrelator(T,TauMax,ThetaMax,i,1,M1,M2,nGap,nPercentGap,1,0,0,0,0,HalfWidth);
    
    ccint(i+1,1) = i + T;
    
    ccint(i+1,2) = IntParams.Qanticustomtheta + IntParams.Qcorrcustomtheta;

end

figure();
plot(ccint(:,1),ccint(:,2));
title(['file1: ', FileName1, 'file2: ', FileName2, ', TStart: ',num2str(TStart),', TEnd: ',num2str(TEnd)]);

%Save the results using the tab-delimited format
button = questdlg('Do you want to save the results?','Results','Yes');
if isequal(button,'Yes')
   [FileName3,PathName3]=uiputfile('*.txt','Specify the output file for cc integral');
     dlmwrite([PathName3,'\',FileName3],[ccint],'delimiter','\t', 'precision', 15);
%   [FileName3,PathName3]=uiputfile('*.txt','Specify the output file for constant tau');
%   dlmwrite([PathName3,'\',FileName3],[n', Q12(:,t2D)],'delimiter','\t', 'precision', 15);   
end