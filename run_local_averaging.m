% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_local_averaging()
% Prompts for the parameters
answer = inputdlg({['# of points to average (odd):'] },...
    'Parameters for local averaging',1,{['5'] });

result = str2double(answer);

nAver = result(1);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Specify the input file');
M1=[PathName1,FileName1]; 

%Loading the data of the first file into a matrix
V=dlmread(M1,'\t',0,0);

len = length(V)-nAver+1;
V1 = zeros(len,2);

for i=1:len
        V1(i,1) = V(i+(nAver-1)/2,1);
        V1(i,2) = mean(V(i:i+nAver-1,2));
end

FileTemp = [FileName1(1:length(FileName1)-4) '-av.txt'];

figure();
plot(V1(:,1),V1(:,2));
title(['filename: ', FileName1, ', size: ',num2str(length(V1)),' points']);

[FileName2,PathName2]=uiputfile('*.txt','Specify the output file',FileTemp);
dlmwrite([PathName2,'\',FileName2],[V1],'delimiter','\t', 'precision', 15);


