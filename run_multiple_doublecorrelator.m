% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Prompts for the parameters
answer = inputdlg({['Averaging interval (T):'] ['End value for tau (in units of T):']...
    ['End value for theta (in units of T):'] ['Start point index:'] ['DC incremement:'] ['Step size:'] ...
    ['Graph: surf(0), contour(1), 2D surf(2)'] ...
    ['Value for missing data (FNS only, -1 - no missing data):']...
    ['Percent of gaps allowed:'],...
    ['Normalized: Yes(1) or No(0)']},...
    'Parameters for double correlator',1,...
    {['400'] ['0.3'] ['0.2'] ['0'] ['200'] ['1'] ['2'] ['-1'] ['25'] ['1']});

result = str2double(answer);

T = result(1);
TauMax = floor(result(2)*T);
ThetaMax = floor(result(3)*T);
TStart = result(4);
DCIncrement = result(5);
DeltaT = result(6);
graphtype = result(7);
nGap = result(8);
nPercentGap = result(9);
nNorm = result(10);

%Prompts for the first file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the first file');
M1=[PathName1,FileName1]; 
%Prompts for the second file name
[FileName2,PathName2] = uigetfile('*.txt','Choose the second file');
M2=[PathName2,FileName2]; 

sep = ',';
fid2 = fopen('doublecorrelator.csv', 'a');
stringtemp = ['Tstart' sep 'q--av' sep 'q-min' sep 'q-+av' sep 'q-max' ...
    sep 'q+-av' sep 'q+min' sep 'q++av' sep 'q+max' ...
    sep 'q0-av' sep 'q0min' sep 'q0+av' sep 'q0max' ...
    sep 'theta' sep 'halfwidth' sep 'qc-av' sep 'qcmin' sep 'qc+av' sep 'qcmax']
fprintf(fid2,stringtemp);
fprintf(fid2,'\n');
fclose(fid2);

%calculate the positions for the figures
bdwidth = 5;
topbdwidth = 30;
set(0,'Units','pixels');
scnsize = get(0,'ScreenSize');

pos1  = [bdwidth,... 
    2*topbdwidth,...
    scnsize(3) - 2*bdwidth,...
    scnsize(4) - 5*topbdwidth];

h1 = figure('Position',pos1,'Name',['Double correlator - ', FileName1, ' - ',FileName2]);

%    title({['T = ',num2str(T),...
%        ', \Deltat = ',num2str(DeltaT), ', FileName1: ',num2str(FileName1),...
%        ', FileName2: ',num2str(FileName2)]});

for i = 1:10
    
    TStartCur = TStart + (i-1).*DCIncrement;

    [m,n,Q12] = doublecorrelator(T,TauMax,ThetaMax,TStartCur,DeltaT,M1,M2,nGap,nPercentGap,nNorm,1,0,0,10,5);

    subplot(2,5,i);
    
    %Type of drawing
    switch graphtype
        case 2,
            surf(m,n,Q12); shading interp;
            view([0 90])
        case 0,
            surf(m,n,Q12); shading interp;
        case 1,
            contour(m,n,Q12,30);
    end
    % 'DataAspectRatio',[length(m)/length(n) 1 0.01]
    set(gca,'linewidth',1,'CLim',[-1 1]);
    title({[' Tstart = ',num2str(TStartCur)]});
    xlim([min(m) max(m)]);
    ylim([min(n) max(n)]);
    if graphtype == 2
        zlim([-1 1]);
    else
        zlim([min(min(Q12)) max(max(Q12))]);
    end
    c=colorbar('vert');
    xlabel('\tau','fontsize',16);
    ylabel('\theta','fontsize',16);
    zlabel('correlation','fontsize',14);
    
end
