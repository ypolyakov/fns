% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_music()
% Prompts for the parameters
answer = inputdlg({['Interval:'] ['Start point index:'] ['Step size:'],...
    ['Length of correlation matrix:']...
    ['Number of resonances to display:']},...
    'Parameters for MUSIC',1,{['2500'] ['0'] ['1'] ['100'] ['5']});

result = str2double(answer);

T = result(1);
TStart = result(2);
DeltaT = result(3);
nTau = result(4);
nReson = result(5);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the first file');
M1=[PathName1,FileName1]; 

%Loading the data of the first file into a matrix
V=dlmread(M1,'\t',0,0);

%gets each DeltaT records
if DeltaT > 1
   Vtmp = V(1:DeltaT:size(V,1),:); 
   clear V;
   V = Vtmp;
   clear Vtmp;
end

N = floor(T/DeltaT);
NStart = floor(TStart/DeltaT);
NTau = floor(nTau/DeltaT);

V1 = V((1+NStart):(1+NStart+N),2);

X=corrmtx(V1,NTau); 
[W,P] = rootmusic(X,nReson*2,[]);
W = abs(W(1:2:length(W)))
P

h1 = figure();
plot(W(1:size(W)),P(1:size(P)),'--rs','LineWidth',2,...
                'MarkerEdgeColor','k',...
                'MarkerFaceColor','g',...
                'MarkerSize',10);
xlabel('resonance frequency','fontsize',16);
ylabel('signal power','fontsize',16);
title(['T = ',num2str(T),', tstart = ',num2str(TStart),...
    ', \Deltat = ',num2str(DeltaT), ', \tau = ', num2str(nTau),...
    ', Resonances: ', num2str(nReson),...
    ', FileName: ',num2str(FileName1)]);

%Save the results using the tab-delimited format
button = questdlg('Do you want to save the resonance frequencies?','Results','Yes');
if isequal(button,'Yes')
   [FileName2,PathName2]=uiputfile('*.txt','Specify the output file');
   dlmwrite([PathName2,'\',FileName2],[W],'delimiter','\t', 'precision', 15);
end


