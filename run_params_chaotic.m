% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_params_chaotic

% Prompts for the parameters
answer = inputdlg({['Averaging interval (T):'] ['End value for tau (in units of T):']...
    ['Start point index:'] ['Step size:'] ...
    ['Minimum frequency used in PS interpolation']...
    ['Maximum frequency used in PS interpolation']...
    ['Value for missing data (FNS only, -1 - no missing data):']...
    ['Percent of gaps allowed:']...
    },... 
    'Parameters for difference moment',1,...    
    {['2500'] ['0.5'] ['0'] ['1'] ['0'] ['0.5'] ['-1'] ['25']});

result = str2double(answer);

T = result(1);
TauMax = floor(result(2)*T);
TStart = result(3);
DeltaT = result(4);
minInterpFreq = result(5);
maxInterpFreq = result(6);
nGap = result(7);
nPercentGap = result(8);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the original data file');
M1=[PathName1,FileName1]; 

%Prompts for the source file name
%[FileName2,PathName2] = uigetfile('*.txt','Choose the power spectrum data file');
%M2=[PathName2,FileName2]; 

%CALCULATIONS FOR THE DIFFERENCE MOMENT
[m, DF, sigm] = diffmoment(T,TauMax,TStart,DeltaT,2,0,M1,nGap,nPercentGap);

VDM = [m; DF]';

%Calculate H1
stDMCLin = [1 1];
ftDMCLin = fittype(['2*H1*x+b'] ,...
     'dependent',{'Fi'},'independent',{'x'},...
     'coefficients',{'H1', 'b'});
 
% Fit this model
cfDMCLin = fit(log10(VDM(2:10,1)),log10(VDM(2:10,2)),ftDMCLin,'Startpoint',stDMCLin);

H1 = cfDMCLin.H1;

% --- Create fit for difference moment
stDMC = [1];
ftDMC = fittype(['2*', num2str(sigm^2), '*gammainc(tau/T1,',num2str(H1),').^2'] ,...
     'dependent',{'Fi'},'independent',{'tau'},...
     'coefficients',{'T1'});
     
% Fit this model using new data
cfDMC = fit(VDM(:,1),VDM(:,2),ftDMC ,'Startpoint',stDMC,'Lower',[0]);

%Calculate the "jump" spectrum component
T1 = cfDMC.T1;

F = @(x) gammainc(x,H1,'upper').^2;
SJ0 = 4*sigm^2*T1*H1*(1-1./(2*H1)*quad(F,0,10000));
PSCJ = @(f) SJ0./(1+(2*pi*10.^f*T1).^(2*H1+1));

%CALCULATIONS FOR THE POWER SPECTRUM
%Loading the data of the first file into a matrix

[mPS,fPS] = powerspectrum(T,TStart,DeltaT,0,TauMax,0,1024,6,0,M1,nGap,nPercentGap);
V1 = [mPS', fPS']; 
%V1 = dlmread(M2,'\t',0,0);

%V1 - V10

if minInterpFreq == 0
    minInterp = 2;
else
    minInterp = floor(minInterpFreq*2*TauMax);
end 

if maxInterpFreq == 0.5
    maxInterp = length(V1)-1;
else
    maxInterp = floor(maxInterpFreq*2*TauMax);
end

VPS = zeros((maxInterp-minInterp+1),1);

VPS(:,1) = log10(V1(minInterp:maxInterp,1));
VPS(:,2) = log10(abs(V1(minInterp:maxInterp,2)));

%extract every hundredth point in log-log scale
LVPS = log10(length(VPS));
LogRange = (0:LVPS/100:LVPS);
%remove duplicate indexes
LogRange = unique(floor(10.^LogRange));
VPSSliced = VPS(LogRange,:);

% --- Create fit for power spectrum (chaotic)
stPSC = [V1(3,2) 2 2 ];
ftPSC = fittype('log10(s0)-log10(1+(2*pi*10.^(f)*T01)^n)' ,...
     'dependent',{'S'},'independent',{'f'},...
     'coefficients',{'s0', 'T01', 'n'});
     
% Fit this model
cfPSC = fit(VPSSliced(:,1),VPSSliced(:,2),ftPSC ,'Startpoint',stPSC,'Lower',[0 0 0]);

% --- Create fit for power spectrum (standard)
stPS = [V1(3,2) 2];
ftPS = fittype('log10(s0)-n*f' ,...
     'dependent',{'S'},'independent',{'f'},...
     'coefficients',{'s0', 'n'});
     
% Fit this model
cfPS = fit(VPSSliced(:,1),VPSSliced(:,2),ftPS ,'Startpoint',stPS,'Lower',[0 0]);

PSCh = PSC(V1(:,1)',cfPSC.s0,cfPSC.T01,cfPSC.n)';
PSCh2 = PSC2(V1(:,1)',cfPS.s0,cfPS.n)';

%calculate the positions for the figures
bdwidth = 5;
topbdwidth = 30;
set(0,'Units','pixels');
scnsize = get(0,'ScreenSize');

pos1  = [bdwidth,... 
    1/3*scnsize(4) + bdwidth,...
    scnsize(3)/2 - 2*bdwidth,...
    scnsize(4)/2 - (topbdwidth + bdwidth)];
pos2 = [pos1(1) + scnsize(3)/2,...
    pos1(2),...
    pos1(3),...
    pos1(4)];

% Plot the power spectrum
h1 = figure('Position',pos1);
loglog(...
    V1(:,1),abs(V1(:,2)),...
    V1(:,1),PSCh,...
    V1(:,1),PSCh2,...
    V1(:,1),PSCJ(log10(V1(:,1))));
title({[' FileName: ',num2str(FileName1),', s0 = ',num2str(cfPSC.s0),', T01 = ',...
    num2str(cfPSC.T01),', n = ',num2str(cfPSC.n),]; ['1/f.s0 = ',num2str(cfPS.s0),...
    ', 1/f.n = ',num2str(cfPS.n)]});
legend('experimental', 'chaotic interpolation', '1/f interpolation','jump component');

% Plot the difference moment
h2 = figure('Position',pos2);
plot(VDM(:,1),VDM(:,2),VDM(:,1),cfDMC(VDM(:,1)));
title({['\sigma = ',num2str(sigm),', H1 = ',...
    num2str(H1),', T1 = ',num2str(cfDMC.T1)];
    ['T = ',num2str(T),', tstart = ',num2str(TStart),...
    ', \Deltat = ',num2str(DeltaT),', FileName: ',num2str(FileName1)]});
legend('experimental', 'chaotic interpolation');

function PSCh = PSC(f,s0,T01,n)
PSCh = s0./(1+(2*pi*f*T01).^n);

function PSCh2 = PSC2(f,s0,n)
PSCh2 = s0./(f.^n);
