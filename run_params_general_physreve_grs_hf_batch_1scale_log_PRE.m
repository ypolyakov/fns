% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_params_general_physreve_grs_hf_batch_1scale_log

% Prompts for the parameters
answer = inputdlg({['Averaging interval (T):']...
    ['Interval of interest (in terms of T): (1)1/9, (2)1/8, (3)1/4, (4)1/2']...
    ['Initial value for T1 (Least Square Fit):']....
    ['Initial value for T01 (Least Square Fit):']...
    ['Number of frequency points to skip (qmin):']...
    ['Number of points used in finding chaotic s0']...
    ['Minimum frequency used in PS interpolation']...
    ['Maximum frequency used in PS interpolation']...    
    ['Use dashed and dot-dashed lines: (0)No, (1)Yes']...
    ['Value for missing data (FNS only, -1 - no missing data):']...
    ['Percent of gaps allowed:']...
    ['Number of points used in fitting:']...
    ['Power spectrum method: (0)AutoCorr., (1)Welch']...
    },...
    'Parameters for parameterization',1,...
    {['40000'] ['1'] ['100'] ['10'] ['0'] ['2'] ['0'] ['0.5'] ['0'] ['-1'] ['25'] ['200'] ['0']});

result = str2double(answer);

T = result(1);
nInter = result(2);
switch nInter
    case 1        
        TM = floor(T/9);
    case 2
        TM = floor(0.125*T);
    case 3
        TM = floor(0.25*T);
    case 4
        TM = floor(0.5*T);        
end
nT1 = result(3);
nT01 = result(4);
nResStart = result(5);
nPointsS0 = result(6);
minInterpFreq = result(7);
maxInterpFreq = result(8);
nLineStyle = result(9);
nGap = result(10);
nPercentGap = result(11);
nInterPoints = result(12);
nPSMethod = result(13);

fid = fopen('grscorrections3.txt');
noise = textscan(fid, '%s %s %s %s %f %f %f %f');
fclose(fid);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the original data file');

fid2 = fopen([PathName1,'results.out'], 'a');

list = dir(fullfile(PathName1,'*.txt'));

for iter = 1:length(list)

    M1=[PathName1,list(iter).name];
    
    series1 = strrep(list(iter).name,'-','');
    series1 = strrep(series1,'_4_','');
    series1 = strrep(series1,'_','');
    series1 = strrep(series1,'detr.txt','');
    series1 = strrep(series1,'g','');

    An = 0;
    Bn = 0;
    Cn = 0;
    Dn = 0;    
    
    for iter2 = 1:length(noise{1})
        series = strrep(cell2mat([noise{1}(iter2),noise{2}(iter2),noise{3}(iter2),noise{4}(iter2)]),'-','');
        if strcmp(series1,series)
           An = noise{5}(iter2);
           Bn = noise{6}(iter2);
           Cn = noise{7}(iter2);
           Dn = noise{8}(iter2);
           break
        end
    end    

    if nPSMethod == 0
        %Calculate the EXPERIMENTAL POWER SPECTRUM using the autocorrelator
        [mPS,PS] = powerspectrum(T,0,1,0,TM,0,1024,6,0,M1,nGap,nPercentGap);
    else
        %Calculate the EXPERIMENTAL POWER SPECTRUM using the Welch method
        [mPS,PS] = powerspectrum(T,0,1,0,TM,0,1024,6,2,M1,nGap,nPercentGap);
    end
    PSExp = [mPS; PS]';
    
    %psSorted = sort(abs(PSExp(length(PSExp)-100:length(PSExp),2)),'descend');
    %PSExpPoisson = mean(psSorted(1:20))
    
    PSExpPoisson = mean(abs(PSExp(length(PSExp)-100:length(PSExp),2)))

    %if An > 0
    %    PSNoise = PSN((1:length(PSExp))',PSExpPoisson*An,PSExpPoisson*Bn,Cn,PSExpPoisson*Dn);    
    %else
    %    ['Error: No noise parameters are given for ', list(iter).name]
    %    break;
    %end
    
    %Calculate the EXPERIMENTAL POWER SPECTRUM using the SQUARE OF THE FOURIER
    %COMPONENT
    %[mPS,PS] = powerspectrum(T,0,1,0,TM,0,1024,6,1,M1,nGap,nPercentGap);
    %PSExpSQF = [mPS; PS]';

    %switch nInter
    %    case 1
    %        nSQFStep = 8;
    %    case 2
    %        nSQFStep = 4;
    %    case 3
    %        nSQFStep = 2;
    %    case 4
    %        nSQFStep = 1;    
    %end
    %PSExpSQF = PSExpSQF(1:nSQFStep:length(PSExpSQF),:);
    
    %PSExp(:,2) = PSExp(:,2) - PSNoise;
    %PSExpSQF(:,2) = PSExpSQF(:,2) - PSNoise;    

    %CHAOTIC POWER SPECTRUM
    if minInterpFreq == 0
        minInterp = 2;
    else
        minInterp = floor(minInterpFreq*2*TM);
    end 

    if maxInterpFreq == 0.5
        maxInterp = length(PSExp)-1;
    else
        maxInterp = floor(maxInterpFreq*2*TM);
    end

    VPS = zeros((maxInterp-minInterp+1),1);
    
    PSExpAdj = PSExp;
    %PSExpAdj(:,2) = abs(abs(PSExp(:,2)) - PSNoise);  
    PSExpAdj(:,2) = abs(PSExp(:,2)); 
    
    VPS(:,1) = log10(PSExp(minInterp:maxInterp,1));
    VPS(:,2) = log10(abs(PSExpAdj(minInterp:maxInterp,2)));

    if nInterPoints > 0
        %extract every hundredth point in log-log scale
        LVPS = log10(length(VPS));
        LogRange = (0:LVPS/nInterPoints:LVPS);
        %remove duplicate indexes
        LogRange = unique(floor(10.^LogRange));
        VPSSliced = VPS(LogRange,:);
    else
        VPSSliced = VPS;
    end

    % find the minimum value of S0
    if minInterpFreq == 0
        S0 = mean(PSExpAdj((nResStart+2):(nResStart+nPointsS0+1),2));
    else
        S0 = PSExpAdj(minInterp,2);
    end

    % --- Create fit for power spectrum (chaotic)
    stPSC = [nT01 2 ];
    ftPSC = fittype(['log10(',num2str(S0),'/(1+(2*pi*10.^(f)*T01)^n)+',num2str(PSExpPoisson),')'] ,...
         'dependent',{'S'},'independent',{'f'},...
         'coefficients',{'T01', 'n'});

    % Fit this model
    cfPSC = fit(VPSSliced(:,1),VPSSliced(:,2),ftPSC ,'Startpoint',stPSC,'Lower',[0 0],'Robust','Off');

    %RESONANT POWER SPECTRUM
    PSRes = zeros(length(PSExp),2);
    PSRes(:,1) = PSExp(:,1);
    PSCh = PSC(PSRes(:,1)',S0,cfPSC.T01,cfPSC.n,PSExpPoisson)';
    %PSRes(:,2) = PSExp(:,2)-PSCh-PSNoise;
    PSRes(:,2) = PSExp(:,2)-PSCh;
    PSRes(1:nResStart,2) = 0;
    %PSRes(1:2,2) = PSExp(1:2,2)-PSNoise(1:2);
    %PSRes(1:1,2) = PSExp(1:1,2);
    PSRes(1:1,2) = PSExp(1:1,2);
    
    %Calculate the AUTOCORRELATOR FOR THE RESONANT COMPONENT
    PSResSym = zeros(2*length(PSRes)-2,1);
    PSResSym(1:length(PSRes)) = PSRes(:,2);
    PSResSym((length(PSRes)+1):(2*length(PSRes)-2)) = PSRes((length(PSRes)-1):-1:2,2)./2;
    PSResSym(2:length(PSRes)-1)=PSResSym(2:length(PSRes)-1)./2;
    ACRes = real(ifft(PSResSym));
    ACRes = ACRes(1:length(PSRes));
    
    nStepSize = 1;
    
    ACRes1 = ACRes; %vertcat(ACRes(1:5,:),ACRes(nStepSize:nStepSize:length(PSRes)));
    
    %Calculate the STRUCTURAL FUNCTION FOR THE RESONANT COMPONENT
    DMRes = 2*(ACRes1(1)-ACRes1);    

    %EXPERIMENTAL DIFFERENCE MOMENT
    [m, DF] = diffmoment(T,length(ACRes),0,nStepSize,2,0,M1,nGap,nPercentGap);
    VDM = [m; DF]';

    %[m1, DF1] = diffmoment(T,5,0,1,2,0,M1,nGap,nPercentGap);
    %VDM1 = [m1; DF1]';
    
    %VDM = vertcat(VDM1,VDM(2:length(VDM),:));   
    
    % --- FIT FOR THE CHAOTIC DIFFERENCE MOMENT
    stDMC = [0.1 10000 0.3 PSExpPoisson];
    ftDMC = fittype(['log10(2*sigm^2*gammainc(10.^(tau)/T1,H1).^2+fi0)'] ,...
         'dependent',{'Fi'},'independent',{'tau'},...
         'coefficients',{'sigm','T1','H1','fi0'});
    
    VPS1 = VDM(2:length(VDM),:); 
     
    VPS1(:,1) = log10(VDM(2:length(VDM),1));
    VPS1(:,2) = log10(abs(VDM(2:length(VDM),2)-DMRes(2:length(VDM))));

    %extract every hundredth point in log-log scale
    LVPS = log10(length(VPS1));
    LogRange = (0:LVPS/50:LVPS);
    %remove duplicate indexes
    LogRange = unique(floor(10.^LogRange));
    VPSSliced = VPS1(LogRange,:);
    
    % Fit this model using new data
    [cfDMC,gof] = fit(VPSSliced(:,1),VPSSliced(:,2),...
        ftDMC ,'Startpoint',stDMC,'Lower',[0 0 0 0],'Upper',[10^10 35555 10 10^10],'Robust','Off');    
    
    DMCh = DMC(VDM(:,1)',cfDMC.sigm,cfDMC.T1,cfDMC.H1,cfDMC.fi0)';        

    temptotal1 = 0;
    for i=1:length(VDM)
       temptotal1 = temptotal1 + abs(VDM(i,2)-DMRes(i)-DMCh(i));
    end

    temptotal2 = 0;
    for i=1:length(VDM)
       temptotal2 = temptotal2 + VDM(i,2);
    end

    RelError = temptotal1./temptotal2;

    Dest = cfDMC.sigm^2/(cfDMC.T1*(gamma(1+cfDMC.H1))^2);

    PSRes1 = PSRes;

    %calculate the positions for the figures
    bdwidth = 5;
    topbdwidth = 30;
    set(0,'Units','pixels');
    scnsize = get(0,'ScreenSize');

    pos1  = [bdwidth,... 
        bdwidth+topbdwidth,...
        scnsize(3)/2 - 2*bdwidth,...
        2*scnsize(4)/5 - (topbdwidth + bdwidth)];
    pos2 = [pos1(1) + scnsize(3)/2,...
        pos1(2),...
        pos1(3),...
        pos1(4)];
    pos3 = [pos1(1),...
        pos1(2) + scnsize(4)/2,...
        pos1(3),...
        pos1(4)];
    pos4 = [pos1(1) + scnsize(3)/2,...
        pos1(2) + scnsize(4)/2,...
        pos1(3),...
        pos1(4)];

    PSExpAbs = PSExp;
    PSExpAbs(:,2) = abs(PSExp(:,2));
    PSResAbs1 = PSRes1;
    PSResAbs1(:,2) = abs(PSRes1(:,2));

    PSExpFiltered = PSExp;
    for i=1:length(PSExp)
        if PSExp(i,2) <= 0
            PSExpFiltered(i,2) = 0;
        end;
    end;
    
    if nLineStyle == 1
        arLineStyle = ['- '; '--'; '-.'; ': '; '- '; '- '];
    else
        arLineStyle = ['-'; '-'; '-'; '-'; '-'; '-'];
    end    
    
    SsT01 = PSC(1/cfPSC.T01,S0,cfPSC.T01,cfPSC.n,PSExpPoisson);

    FileName1 = list(iter).name;
    
    close all;
    
    % Plot the power spectrum
    h1 = figure('Position',pos3);
    loglog(PSExp((nResStart+1):length(PSExp),1),PSExpAdj((nResStart+1):length(PSExp),2),'-b',...
        PSExp((nResStart+1):length(PSExp),1),PSCh((nResStart+1):length(PSExp)),'-r'... 
    );
        %PSExp((nResStart+1):length(PSExp),1),...
        %abs(PSRes1((nResStart+1):length(PSExp),2)+PSCh((nResStart+1):length(PSExp))),arLineStyle(2,:),...
    title({[num2str(FileName1),', Ss0 = ',num2str(S0),', T01 = ',...
        num2str(cfPSC.T01),', n = ',num2str(cfPSC.n)]; ['T = ',num2str(T),...
        ',TM = ',num2str(TM),', H1 = ',...
        num2str(cfDMC.H1),', T1 = ',num2str(cfDMC.T1),', SsT01 = ',num2str(SsT01)]; });
    %legend('experimental (autocorrelator)', 'general interpolation','chaotic');
    xlabel('f','fontsize',16);
    ylabel('power spectrum','fontsize',16);

    % Plot the difference moment
    h2 = figure('Position',pos1);
    plot(VDM(:,1),VDM(:,2),arLineStyle(1,:),...
        VDM(:,1),DMCh(1:length(VDM))+DMRes(1:length(VDM)),arLineStyle(2,:),...
        VDM(:,1),DMRes(1:length(VDM)),arLineStyle(3,:)); 
    title({['\sigma = ',num2str(cfDMC.sigm),', H1 = ',...
        num2str(cfDMC.H1),', T1 = ',num2str(cfDMC.T1),', Relative Error = ', num2str(RelError)];
        ['T = ',num2str(T),', qMin = ',num2str(nResStart),', FileName: ',num2str(FileName1)]});
    %legend('experimental', 'general interpolation','resonant interpolation');
    xlabel('\tau','fontsize',16);
    ylabel('difference moment','fontsize',16);

    % Plot the power spectrum - autocorrelator vs power spectrums
    % h3 = figure('Position',pos4);
    % loglog(PSExpSQF(2:length(PSExpSQF),1),PSExpSQF(2:length(PSExpSQF),2),arLineStyle(1,:),...
    %     PSExp(2:length(PSExpSQF),1),PSExpAbs(2:length(PSExpSQF),2),arLineStyle(2,:));
    % title({['Comparison of power spectrums'];...
    %     ['FileName = ',num2str(FileName1),', T = ',num2str(T),',TM = ',num2str(TM)]});
    % %legend('periodogram','abs(autocorrelator)');
    % xlabel('f','fontsize',16);
    % ylabel('power spectrum','fontsize',16);

    % Plot the chaotic difference moment
    h4 = figure('Position',pos2);
    loglog(VDM(:,1),VDM(:,2)-DMRes(1:length(VDM)),arLineStyle(1,:),VDM(:,1),DMCh,arLineStyle(2,:));
    title({['\sigma = ',num2str(cfDMC.sigm),', H1 = ',...
        num2str(cfDMC.H1),', T1 = ',num2str(cfDMC.T1), ', D = ', num2str(Dest),', Relative Error = ', num2str(RelError)];
        ['T = ',num2str(T),', qMin = ',num2str(nResStart),', FileName: ',num2str(FileName1)]});
    %legend('experimental-resonant', 'chaotic interpolation');
    xlabel('\tau','fontsize',16);
    ylabel('chaotic difference moment','fontsize',16);
    
    stringtemp = [list(iter).name char(9) num2str(S0) ...
    char(9) num2str(SsT01) char(9) num2str(cfPSC.n) char(9) num2str(cfPSC.T01) ... 
     char(9) num2str(cfDMC.sigm) char(9) num2str(cfDMC.H1) char(9) num2str(cfDMC.T1)...
    char(9) num2str(Dest) char(9) num2str(RelError)]

    %stringtemp = [list(iter).name '\t' num2str(cfDMC.sigm) '\t' num2str(cfDMC.H1) '\t' num2str(cfDMC.T1)...
    %'\t' num2str(Dest) '\t' num2str(S0) ...
    %'\t' num2str(SsT01) '\t' num2str(cfPSC.n) '\t' num2str(cfPSC.T01) '\t' num2str(RelError)]    
    
    fOutputh1 = strrep(M1, '.txt', '-PS.png');
    fOutputh2 = strrep(M1, '.txt', '-DM.png');
    fOutputh3 = strrep(M1, '.txt', '-PS2.png');
    fOutputh4 = strrep(M1, '.txt', '-CDM.png');
    
    set(h1,'PaperUnits','inches');
    set(h1, 'PaperPosition', [0 0 6 4]);
    print(h1,fOutputh1,'-dpng','-r300','-opengl');
    set(h2,'PaperUnits','inches');
    set(h2, 'PaperPosition', [0 0 6 4]);    
    print(h2,fOutputh2,'-dpng','-r300','-opengl');
%    set(h3,'PaperUnits','inches');
%    set(h3, 'PaperPosition', [0 0 6 4]);    
%    print(h3,fOutputh3,'-dpng','-r300','-opengl');
    set(h4,'PaperUnits','inches');
    set(h4, 'PaperPosition', [0 0 6 4]);    
    print(h4,fOutputh4,'-dpng','-r300','-opengl');
    
    fprintf(fid2,stringtemp);
    fprintf(fid2,'\n');
    
end

fclose(fid2);

function PSCh = PSC(f,s0,T01,n,p)
PSCh = s0./(1+(2*pi*f*T01).^n)+p;

function DMCh = DMC(tau,sigm,T1,H1,p)
DMCh = 2*sigm^2*(gammainc(tau/T1,H1)).^2+p;

function PSNoise = PSN(q,A,B,C,D)
PSNoise = A-B.*cos(C.*q)+D;