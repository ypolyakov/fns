% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_params_general_physreve_v1

% Prompts for the parameters
answer = inputdlg({['Averaging interval (T):']...
    ['Interval of interest (in terms of T): (2)0.125, (3)0.25, (4)0.5']...
    ['Initial value for T1 (Least Square Fit):']....
    ['Initial value for T01 (Least Square Fit):']...
    ['Number of frequency points to skip (qmin):']...
    ['Number of points used in finding chaotic s0']...
    ['Minimum frequency used in PS interpolation']...
    ['Maximum frequency used in PS interpolation']...    
    ['Use dashed and dot-dashed lines: (0)No, (1)Yes']...
    ['Value for missing data (FNS only, -1 - no missing data):']...
    ['Percent of gaps allowed:']...
    ['Number of points used in fitting:']...
    },...
    'Parameters for parameterization',1,...
    {['40000'] ['3'] ['100'] ['10'] ['0'] ['2'] ['0'] ['0.5'] ['0'] ['-1'] ['25'] ['200']});

result = str2double(answer);

T = result(1);
nInter = result(2);
switch nInter
    case 1        
        TM = floor(0.0625*T);
    case 2
        TM = floor(0.125*T);
    case 3
        TM = floor(0.25*T);
    case 4
        TM = floor(0.5*T);        
end
nT1 = result(3);
nT01 = result(4);
nResStart = result(5);
nPointsS0 = result(6);
minInterpFreq = result(7);
maxInterpFreq = result(8);
nLineStyle = result(9);
nGap = result(10);
nPercentGap = result(11);
nInterPoints = result(12);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the original data file');
M1=[PathName1,FileName1]; 

%Calculate the EXPERIMENTAL POWER SPECTRUM using the autocorrelator
[mPS,PS] = powerspectrum(T,0,1,0,TM,0,1024,6,0,M1,nGap,nPercentGap);
PSExp = [mPS; PS]';

%Calculate the EXPERIMENTAL POWER SPECTRUM using the SQUARE OF THE FOURIER
%COMPONENT
[mPS,PS] = powerspectrum(T,0,1,0,TM,0,1024,6,1,M1,nGap,nPercentGap);
PSExpSQF = [mPS; PS]';

switch nInter
    case 1
        nSQFStep = 8;
    case 2
        nSQFStep = 4;
    case 3
        nSQFStep = 2;
    case 4
        nSQFStep = 1;    
end
PSExpSQF = PSExpSQF(1:nSQFStep:length(PSExpSQF),:);

%CHAOTIC POWER SPECTRUM
if minInterpFreq == 0
    minInterp = 2;
else
    minInterp = floor(minInterpFreq*2*TM);
end 

if maxInterpFreq == 0.5
    maxInterp = length(PSExp)-1;
else
    maxInterp = floor(maxInterpFreq*2*TM);
end

VPS = zeros((maxInterp-minInterp+1),1);

VPS(:,1) = log10(PSExp(minInterp:maxInterp,1));
VPS(:,2) = log10(abs(PSExp(minInterp:maxInterp,2)));

if nInterPoints > 0
    %extract every hundredth point in log-log scale
    LVPS = log10(length(VPS));
    LogRange = (0:LVPS/nInterPoints:LVPS);
    %remove duplicate indexes
    LogRange = unique(floor(10.^LogRange));
    VPSSliced = VPS(LogRange,:);
else
    VPSSliced = VPS;
end

% find the minimum value of S0
if minInterpFreq == 0
    S0 = mean(abs(PSExp((nResStart+2):(nResStart+nPointsS0+1),2)));
else
    S0 = abs(PSExp(minInterp,2));
end

% --- Create fit for power spectrum (chaotic)
stPSC = [nT01 2 ];
ftPSC = fittype(['log10(',num2str(S0),')-log10(1+(2*pi*10.^(f)*T01)^n)'] ,...
     'dependent',{'S'},'independent',{'f'},...
     'coefficients',{'T01', 'n'});
     
% Fit this model
cfPSC = fit(VPSSliced(:,1),VPSSliced(:,2),ftPSC ,'Startpoint',stPSC,'Lower',[0 0],'Robust','Off');

%RESONANT POWER SPECTRUM
PSRes = zeros(length(PSExp),2);
PSRes(:,1) = PSExp(:,1);
PSCh = PSC(PSRes(:,1)',S0,cfPSC.T01,cfPSC.n)';
PSRes(:,2) = PSExp(:,2)-PSCh;
PSRes(1:nResStart,2) = 0;

%Calculate the AUTOCORRELATOR FOR THE RESONANT COMPONENT
PSResSym = zeros(2*length(PSRes)-2,1);
PSResSym(1:length(PSRes)) = PSRes(:,2);
PSResSym((length(PSRes)+1):(2*length(PSRes)-2)) = PSRes((length(PSRes)-1):-1:2,2)./2;
PSResSym(2:length(PSRes)-1)=PSResSym(2:length(PSRes)-1)./2;
ACRes = real(ifft(PSResSym));
ACRes = ACRes(1:length(PSRes));

%Calculate the STRUCTURAL FUNCTION FOR THE RESONANT COMPONENT
DMRes = 2*(ACRes(1)-ACRes);

%EXPERIMENTAL DIFFERENCE MOMENT
[m, DF] = diffmoment(T,length(DMRes),0,1,2,0,M1,nGap,nPercentGap);
VDM = [m; DF]';

% --- FIT FOR THE CHAOTIC DIFFERENCE MOMENT
stDMC = [0.1 nT1 0.3];
ftDMC = fittype(['2*sigm^2*gammainc(tau/T1,H1).^2'] ,...
     'dependent',{'Fi'},'independent',{'tau'},...
     'coefficients',{'sigm','T1','H1'});

sDM = 1;

% Fit this model using new data
[cfDMC,gof] = fit(VDM(1:sDM:length(VDM),1),VDM(1:sDM:length(VDM),2)-DMRes(1:sDM:length(VDM)),...
    ftDMC ,'Startpoint',stDMC,'Lower',[0 0 0],'Robust','Off');

temptotal1 = 0;
for i=1:length(VDM)
   temptotal1 = temptotal1 + abs(VDM(i,2)-DMRes(i)-cfDMC(VDM(i,1)));
end

temptotal2 = 0;
for i=1:length(VDM)
   temptotal2 = temptotal2 + VDM(i,2);
end

RelError = temptotal1./temptotal2;

Dest = cfDMC.sigm^2/(cfDMC.T1*(gamma(1+cfDMC.H1))^2);

PSRes1 = PSRes;

% Calculate the JUMP COMPONENT OF THE POWER SPECTRUM using the analytical
% expression
[SJ0,PSChJ] = PSJ(PSExp(:,1)',cfDMC.sigm,cfDMC.T1,cfDMC.H1);
PSChJ = PSChJ';
PSChJ(2:length(PSChJ)-1) = 2*PSChJ(2:length(PSChJ)-1);

PSChTemp = PSCh - PSChJ;

% Calculate the spike component of the power spectrum
VPS = zeros((maxInterp-minInterp+1),1);

VPS(:,1) = log10(PSExp(minInterp:maxInterp,1));
VPS(:,2) = log10(PSChTemp(minInterp:maxInterp)); 

if nInterPoints > 0
    %extract every hundredth point in log-log scale
    LVPS = log10(length(VPS));
    LogRange = (0:LVPS/nInterPoints:LVPS);
    %remove duplicate indexes
    LogRange = unique(floor(10.^LogRange));
    VPSSliced = VPS(LogRange,:);
else
    VPSSliced = VPS;
end

% --- Create fit for power spectrum (chaotic)
stPSSp = [0.5*S0 cfPSC.T01 cfPSC.n ];
ftPSSp = fittype(['log10((SR0)./(1+(2*pi*10.^(f)*T0)^n0))'] ,...
     'dependent',{'S'},'independent',{'f'},...
     'coefficients',{'SR0','T0', 'n0'});
     
% Fit this model
cfPSSp = fit(VPSSliced(:,1),VPSSliced(:,2),ftPSSp ,'Startpoint',stPSSp,'Lower',[0 0 0],'Upper',[S0 10^10 10],'Robust','Off'); 

%new added 05/12/2010

VPS = zeros((maxInterp-minInterp+1),1);

VPS(:,1) = log10(PSExp(minInterp:maxInterp,1));
VPS(:,2) = log10(abs(PSExp(minInterp:maxInterp,2)));

if nInterPoints > 0
    %extract every hundredth point in log-log scale
    LVPS = log10(length(VPS));
    LogRange = (0:LVPS/nInterPoints:LVPS);
    %remove duplicate indexes
    LogRange = unique(floor(10.^LogRange));
    VPSSliced = VPS(LogRange,:);
else
    VPSSliced = VPS;
end

%['log10((',num2str(S0),'-SS0)/(1+(2*pi*10.^(f)*',num2str(cfDMC.T1),')^(2.*',num2str(cfDMC.H1),...
%    '+1))+SS0/(1+(2*pi*10.^(f)*T01)^',num2str(cfPSSp.n0),'))']

% --- Create fit for power spectrum (chaotic)
stPSC2 = [nT01 0.5.*S0];
ftPSC2 = fittype(['log10((',num2str(S0),'-SS0)/(1+(2*pi*10.^(f)*',num2str(cfDMC.T1),')^(2.*',num2str(cfDMC.H1),...
    '+1))+SS0/(1+(2*pi*10.^(f)*T0)^',num2str(cfPSSp.n0),'))'],...
     'dependent',{'S'},'independent',{'f'},...
     'coefficients',{'T0', 'SS0'});
     
% Fit this model
cfPSC2 = fit(VPSSliced(:,1),VPSSliced(:,2),ftPSC2 ,'Startpoint',stPSC2,'Lower',[1 0],'Upper',[10^7 S0],'Robust','Off')

PSChJ = PSC(PSExp(:,1)',S0-cfPSC2.SS0,cfDMC.T1,2*cfDMC.H1+1)';
PSChS = PSC(PSExp(:,1)',cfPSC2.SS0,cfPSC2.T0,cfPSSp.n0)';

temptotal1 = 0;
for i=(nResStart+2):length(PSExp)
   temptotal1 = temptotal1 + abs(PSExp(i,2)-(PSRes(i,2)+PSChS(i)+PSChJ(i)));
end

temptotal2 = 0;
for i=(nResStart+2):length(PSExp)
   temptotal2 = temptotal2 + abs(PSExp(i,2));
end

RelErrorS = temptotal1./temptotal2;

%calculate the positions for the figures
bdwidth = 5;
topbdwidth = 30;
set(0,'Units','pixels');
scnsize = get(0,'ScreenSize');

pos1  = [bdwidth,... 
    bdwidth+topbdwidth,...
    scnsize(3)/2 - 2*bdwidth,...
    2*scnsize(4)/5 - (topbdwidth + bdwidth)];
pos2 = [pos1(1) + scnsize(3)/2,...
    pos1(2),...
    pos1(3),...
    pos1(4)];
pos3 = [pos1(1),...
    pos1(2) + scnsize(4)/2,...
    pos1(3),...
    pos1(4)];
pos4 = [pos1(1) + scnsize(3)/2,...
    pos1(2) + scnsize(4)/2,...
    pos1(3),...
    pos1(4)];

PSExpAbs = PSExp;
PSExpAbs(:,2) = abs(PSExp(:,2));
PSResAbs1 = PSRes1;
PSResAbs1(:,2) = abs(PSRes1(:,2));

PSExpFiltered = PSExp;
for i=1:length(PSExp)
    if PSExp(i,2) <= 0
        PSExpFiltered(i,2) = 0;
    end;
end;

SsT01 = PSC(1/cfPSC.T01,S0,cfPSC.T01,cfPSC.n);
SsJT01 = PSC(1/cfPSC.T01,S0-cfPSC2.SS0,cfDMC.T1,2*cfDMC.H1+1);;
SsST01 = PSC(1/cfPSC.T01,cfPSC2.SS0,cfPSC2.T0,cfPSSp.n0);
SsT0 = PSC(1/cfPSC2.T0,S0,cfPSC.T01,cfPSC.n);
SsJT0 = PSC(1/cfPSC2.T0,S0-cfPSC2.SS0,cfDMC.T1,2*cfDMC.H1+1);
SsST0 = PSC(1/cfPSC2.T0,cfPSC2.SS0,cfPSC2.T0,cfPSSp.n0);

if nLineStyle == 1
    arLineStyle = ['- '; '--'; '-.'; ': '; '- '; '- '];
else
    arLineStyle = ['-'; '-'; '-'; '-'; '-'; '-'];
end

% Plot the power spectrum
h1 = figure('Position',pos3);
loglog(PSExp((nResStart+1):length(PSExp),1),PSExpAbs((nResStart+1):length(PSExp),2),arLineStyle(1,:),...
    PSExp((nResStart+1):length(PSExp),1),...
    abs(PSRes1((nResStart+1):length(PSExp),2)+PSChS((nResStart+1):length(PSExp))+PSChJ((nResStart+1):length(PSExp))),arLineStyle(2,:),...
    PSExp((nResStart+1):length(PSExp),1),PSCh((nResStart+1):length(PSExp)),arLineStyle(3,:),...     
    PSExp((nResStart+1):length(PSExp),1),PSChJ((nResStart+1):length(PSExp)),arLineStyle(4,:),... 
    PSExp((nResStart+1):length(PSExp),1),PSChS((nResStart+1):length(PSExp)),arLineStyle(5,:),...   
    PSExp((nResStart+1):length(PSExp),1),PSChS((nResStart+1):length(PSExp))+PSChJ((nResStart+1):length(PSExp)),arLineStyle(6,:)... 
);
title({[' FileName: ',num2str(FileName1),', Ss0 = ',num2str(S0),', T01 = ',...
    num2str(cfPSC.T01),', n = ',num2str(cfPSC.n)]; ['T = ',num2str(T),...
    ',TM = ',num2str(TM),', SsJ0 = ',num2str(S0-cfPSC2.SS0), ', H1 = ',...
    num2str(cfDMC.H1),', T1 = ',num2str(cfDMC.T1)]; ['SsS0 = ',num2str(cfPSC2.SS0), ', n0 = ',...
    num2str(cfPSSp.n0),', T0 = ',num2str(cfPSC2.T0)]; 
    ['SsT01 = ',num2str(SsT01), ', SsST0 = ',...
    num2str(SsST0),', RelErrorS = ',num2str(RelErrorS)]; });
legend('experimental (autocorrelator)', 'general interpolation','chaotic','jump', 'spike', 'jump + spike');
xlabel('f','fontsize',16);
ylabel('power spectrum','fontsize',16);
%axis([10^(-4)  1  0.001  10*max(PSExp(:,2))])

% Plot the difference moment
h2 = figure('Position',pos1);
plot(VDM(:,1),VDM(:,2),arLineStyle(1,:),...
    VDM(:,1),cfDMC(VDM(:,1))+DMRes,arLineStyle(2,:),...
    VDM(:,1),DMRes,arLineStyle(3,:)); 
title({['\sigma = ',num2str(cfDMC.sigm),', H1 = ',...
    num2str(cfDMC.H1),', T1 = ',num2str(cfDMC.T1),', Relative Error = ', num2str(RelError)];
    ['T = ',num2str(T),', qMin = ',num2str(nResStart),', FileName: ',num2str(FileName1)]});
legend('experimental', 'general interpolation','resonant interpolation');
xlabel('\tau','fontsize',16);
ylabel('difference moment','fontsize',16);

% Plot the power spectrum - autocorrelator vs power spectrums
h31 = figure('Position',pos4);
loglog(PSExpSQF(2:length(PSExpSQF),1),PSExpSQF(2:length(PSExpSQF),2),arLineStyle(1,:),...
    PSExp(2:length(PSExpSQF),1),PSExpAbs(2:length(PSExpSQF),2),arLineStyle(2,:),...
    PSExp(2:length(PSExpSQF),1),PSExpFiltered(2:length(PSExpSQF),2),arLineStyle(3,:));
title({['Comparison of power spectrums'];...
    ['FileName = ',num2str(FileName1),', T = ',num2str(T),',TM = ',num2str(TM)]});
legend('periodogram','abs(autocorrelator)','autocorrelator');
xlabel('f','fontsize',16);
ylabel('power spectrum','fontsize',16);

% Plot the chaotic difference moment
h4 = figure('Position',pos2);
plot(VDM(:,1),VDM(:,2)-DMRes,arLineStyle(1,:),VDM(:,1),cfDMC(VDM(:,1)),arLineStyle(2,:));
title({['\sigma = ',num2str(cfDMC.sigm),', H1 = ',...
    num2str(cfDMC.H1),', T1 = ',num2str(cfDMC.T1), ', D = ', num2str(Dest),', Relative Error = ', num2str(RelError)];
    ['T = ',num2str(T),', qMin = ',num2str(nResStart),', FileName: ',num2str(FileName1)]});
legend('experimental-resonant', 'chaotic interpolation');
xlabel('\tau','fontsize',16);
ylabel('chaotic difference moment','fontsize',16);

%cfPSC
%cfDMC
%cfPSSp

[FileName1 char(9) num2str(cfDMC.sigm) char(9) num2str(cfDMC.H1) char(9) num2str(cfDMC.T1)...
    char(9) num2str(Dest) char(9) num2str(cfPSC.n) char(9) num2str(S0) char(9) num2str(S0-cfPSC2.SS0) char(9) num2str(cfPSC2.SS0) ...
    char(9) num2str(SsT01) char(9) num2str(SsJT01) char(9) num2str(SsST01)  ...
    char(9) num2str(SsT0) char(9) num2str(SsJT0) char(9) num2str(SsST0) ...  
    char(9) num2str(cfPSC.T01) char(9) num2str(cfPSSp.n0) char(9) num2str(cfPSC2.T0)...
    char(9) num2str(RelError) char(9) num2str(RelErrorS)]

function PSCh = PSC(f,s0,T01,n)
PSCh = s0./(1+(2*pi*f*T01).^n);

function [SJ0,PSChJ] = PSJ(f,sigm,T1,H1)
F = @(x) gammainc(x,H1,'upper').^2;
SJ0 = 4*sigm^2*T1*H1*(1-1./(2*H1)*quad(F,0,10000));
if H1 > 0.5
    PSChJ = SJ0./(1+(2*pi*f*T1).^(2*H1+1));
else
    PSChJ = SJ0./(1+(2*pi*f*T1).^(2*H1+1)).*(1+(-5.7*H1^3+2.8*H1^2-0.20*H1+0.66).*(1-exp(-(-6.5*H1^3+74*H1^2-54*H1+12).*f.*T1)));
end