% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Prompts for the parameters
answer = inputdlg({['Averaging interval:'] ['Start point index:'] ['Step size:'],...
    ['Number of lowest frequency points not to display']...
    ['Number of frequency points to calculate (in units of T)']...
    ['Normalization (FNS only): On(1), Off(0) ']...
    ['Absolute value of spectrum (FNS only): On(1), Off(0) ']...    
    ['Size of FFT (Burg and Yule-Walker only)']...
    ['Order of AR prediction model (Burg and Yule-Walker only)']...    
    ['Type: FNS(0), FFT(1), Welch(2), Burg(3), Yule-Walker(4)']...
    ['Value for missing data (FNS only, -1 - no missing data):']...
    ['Percent of gaps allowed:']},...
    'Parameters for power spectrum',1,{['4500'] ['0'] ['1'] ['0'] ['0.25'] ['0'] ['1'] ['1024'] ['6'] ['0'] ['-1'] ['25']});

result = str2double(answer);

T = result(1);
TStart = result(2);
DeltaT = result(3);
nSkip = result(4);
nTotal = floor(result(5)*T);
nNorma = result(6);
nAbs = result(7);
nSize = result(8);
nOrder = result(9);
nType = result(10);
nGap = result(11);
nPercentGap = result(12);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the source file');
M1=[PathName1,FileName1]; 

[m, PS] = powerspectrum(T,TStart,DeltaT,nSkip,nTotal,nNorma,nSize,nOrder,nType,M1,nGap,nPercentGap);

switch nType
    case 0,
        sType = 'FNS-FFT';  
    case 1,
        sType = 'FFT-standard';        
    case 2,
        sType = 'Welch';                
    case 3,
        sType = 'Burg';   
    case 4,
        sType = 'Yule-Walker';  
    case 5,
        sType = 'FNS-Cosine';          
end

%calculate the positions for the figures
bdwidth = 5;
topbdwidth = 30;
set(0,'Units','pixels');
scnsize = get(0,'ScreenSize');

pos1  = [bdwidth,... 
    1/3*scnsize(4) + bdwidth,...
    scnsize(3)/2 - 2*bdwidth,...
    scnsize(4)/2 - (topbdwidth + bdwidth)];
pos2 = [pos1(1) + scnsize(3)/2,...
    pos1(2),...
    pos1(3),...
    pos1(4)];

if nAbs == 1
   PS = abs(PS); 
end

%plot the figures
h1 = figure('Position',pos1);
plot(m,PS);
xlabel('f','fontsize',16);
ylabel('power spectrum','fontsize',16);
title({['Definition = ',sType,', T = ',num2str(T), ', tstart = ',num2str(TStart),...
    ', \Deltat = ',num2str(DeltaT), ', FileName: ',num2str(FileName1)];
    ['FNS points = ',num2str(nTotal),', Burg FFT Size = ',num2str(nSize),', Burg Order = ',num2str(nOrder)]});

h2  = figure('Position',pos2);
loglog(m,abs(PS));
xlabel('f','fontsize',16);
ylabel('power spectrum','fontsize',16);
title({['Definition = ',sType,', T = ',num2str(T), ', tstart = ',num2str(TStart),...
    ', \Deltat = ',num2str(DeltaT), ', FileName: ',num2str(FileName1)];
    ['FNS points = ',num2str(nTotal),', Burg FFT Size = ',num2str(nSize),', Burg Order = ',num2str(nOrder)]});

%Save the results using the tab-delimited format
button = questdlg('Do you want to save the results?','Results','Yes');
if isequal(button,'Yes')
   [FileName2,PathName2]=uiputfile('*.txt','Specify the output file');
   dlmwrite([PathName2,'\',FileName2],[m', PS'],'delimiter','\t', 'precision', 15);
end

