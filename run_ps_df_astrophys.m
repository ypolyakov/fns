% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_ps_df_astrophys

% Prompts for the parameters
answer = inputdlg({['Averaging interval (T):']...
    ['Interval of interest (in terms of T): (1)0.125, (2)0.25, (3)0.5']...
    ['Start point index:']...
    ['Value for missing data (FNS only, -1 - no missing data):']...
    ['Percent of gaps allowed:']...
    },...
    'Input parameters',1,...
    {['2000'] ['3'] ['0'] ['0'] ['25']});

result = str2double(answer);

T = result(1);
nInter = result(2);
switch nInter
    case 1        
        TM = floor(0.125*T);
    case 2
        TM = floor(0.25*T);
    case 3
        TM = floor(0.5*T);
end
TStart = result(3);
nGap = result(4);
nPercentGap = result(5);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Choose the original data file');
M1=[PathName1,FileName1]; 

%Loading the data of the source file into a matrix
V=dlmread(M1,'\t',0,0);

%Calculate the EXPERIMENTAL POWER SPECTRUM using the autocorrelator
[mPS,PS] = powerspectrum(T,TStart,1,0,TM,0,1024,6,0,M1,nGap,nPercentGap);
PSExp = [mPS; PS]';

%Calculate the EXPERIMENTAL POWER SPECTRUM using the SQUARE OF THE FOURIER
%COMPONENT
[mPS,PS] = powerspectrum(T,TStart,1,0,TM,0,1024,6,1,M1,nGap,nPercentGap);
PSExpSQF = [mPS; PS]';

%switch nInter
%    case 1
%        nSQFStep = 4;
%    case 2
%        nSQFStep = 2;
%    case 3
%        nSQFStep = 1;
%end
%PSExpSQF = PSExpSQF(1:nSQFStep:length(PSExpSQF),:);

%EXPERIMENTAL DIFFERENCE MOMENT
[m, DF] = diffmoment(T,TM,TStart,1,2,0,M1,nGap,nPercentGap);
VDM = [m; DF]';


%calculate the positions for the figures
bdwidth = 5;
topbdwidth = 30;
set(0,'Units','pixels');
scnsize = get(0,'ScreenSize');

pos1  = [bdwidth,... 
    bdwidth+topbdwidth,...
    scnsize(3)/2 - 2*bdwidth,...
    2*scnsize(4)/5 - (topbdwidth + bdwidth)];
pos2 = [pos1(1) + scnsize(3)/2,...
    pos1(2),...
    pos1(3),...
    pos1(4)];
pos3 = [pos1(1),...
    pos1(2) + scnsize(4)/2,...
    pos1(3),...
    pos1(4)];
pos4 = [pos1(1) + scnsize(3)/2,...
    pos1(2) + scnsize(4)/2,...
    pos1(3),...
    pos1(4)];

PSExpAbs = PSExp;
PSExpAbs(:,2) = abs(PSExp(:,2));

PSExpFiltered = PSExp;
for i=1:length(PSExp)
    if PSExp(i,2) <= 0
        PSExpFiltered(i,2) = 0;
    end;
end;

% Plot the power spectrum - autocorrelator vs power spectrums
h31 = figure('Position',pos3);
plot(PSExpSQF(2:length(PSExpSQF),1),PSExpSQF(2:length(PSExpSQF),2),...
    PSExp(2:length(PSExp),1),PSExpFiltered(2:length(PSExp),2));
title({['Comparison of power spectrums'];...
    ['FileName = ',num2str(FileName1),', T = ',num2str(T),', TM = ',num2str(TM),', TStart = ',num2str(TStart)]});
legend('periodogram','autocorrelator');
xlim([0 0.02]);
xlabel('f','fontsize',16);
ylabel('power spectrum','fontsize',16);

% Plot the difference moment
h2 = figure('Position',pos4);
plot(VDM(:,1),VDM(:,2)); 
title({
    ['T = ',num2str(T),', TStart = ',num2str(TStart),', FileName: ',num2str(FileName1)]});
xlabel('\tau','fontsize',16);
ylabel('difference moment','fontsize',16);


