% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_show_gaps()
% Prompts for the parameters
answer = inputdlg({['Inactivity value:'] },...
    'Parameters gap display',1,{['0'] });

result = str2double(answer);

VInactive = result(1);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Specify the input file');
M1=[PathName1,FileName1]; 

%Loading the data of the first file into a matrix
V=dlmread(M1,'\t',0,0);

ind = find(V(:,2)==VInactive);
V(ind,:) = [];
['gap starts: ', num2str(ind(1))]
for i = 2:length(ind)-1
    if ind(i)-ind(i-1) > 1
        ['gap ends: ', num2str(ind(i-1))]
        ['gap starts: ', num2str(ind(i))]
    end;
end;
['gap ends: ', num2str(ind(length(ind)))]



