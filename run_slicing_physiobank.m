% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_slicing_physiobank()
% Prompts for the parameters
answer = inputdlg({['Interval start point:'] ['Interval end point:'] ['Step size:']},...
    'Parameters for data extraction',1,{['256'] ['921600'] ['256'] });

result = str2double(answer);

TStart = result(1);
TEnd = result(2);
DeltaT = result(3);

%Prompts for the source file name
[FileName1,PathName1] = uigetfile('*.txt','Specify the input file');
M1=[PathName1,FileName1]; 

%Loading the data of the first file into a matrix
V=dlmread(M1,',',1,0);

V1 = V(TStart:DeltaT:TEnd,:);

FileNameSign = [FileName1(1:length(FileName1)-8) 'signals.txt'];

M2=[PathName1,FileNameSign];

fid = fopen(M2);
V2 = textscan(fid, '%s%s%*[^\n]','delimiter',',');
fclose(fid);

for iter = 2:length(V2{2})
    FileTemp = [FileName1(1:length(FileName1)-4) '-' strtrim(cell2mat(V2{2}(iter))) '.txt'];
    dlmwrite([PathName1,'\',FileTemp],[V1(:,1),V1(:,iter)], 'delimiter','\t', 'precision', 15);    
end




