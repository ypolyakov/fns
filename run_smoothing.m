% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function run_smoothing()
% Prompts for the parameters
answer = inputdlg({['Algorithm: (0)-Explicit finite difference; (1) - MATLAB:']...
    ['Diffusion number (omega):'] ['Number of iterations:']},...
    'Parameters for smoothing',1,...
    {['0'] ['0.25'] ['10']});

result = str2double(answer);

nAlg = result(1);
omega = result(2);
IterNo = floor(result(3));

%Prompts for the source file name
[FileName1,PathName1] = uigetfile({'*.txt', 'Text files (*.txt)'} ,'Choose the first file');
M1=[PathName1,FileName1]; 

if nAlg == 0
    [m, HFS, LFS] = smoothing(omega,IterNo,M1);
else
    [m, HFS, LFS] = smoothing_pdepe(omega,IterNo,M1);
end

%calculate the positions for the figures
bdwidth = 5;
topbdwidth = 30;
set(0,'Units','pixels');
scnsize = get(0,'ScreenSize');

pos1  = [bdwidth,... 
    1/3*scnsize(4) + bdwidth,...
    scnsize(3)/2 - 2*bdwidth,...
    scnsize(4)/2 - (topbdwidth + bdwidth)];
pos2 = [pos1(1) + scnsize(3)/2,...
    pos1(2),...
    pos1(3),...
    pos1(4)];

%plot the figures
h1 = figure('Position',pos1);
plot(m,HFS);
xlabel('t','fontsize',16);
ylabel('High-Frequency Component','fontsize',16);
title(['\omega = ',num2str(omega),', Iteration number = ',num2str(IterNo)...
    ', FileName: ',num2str(FileName1)]);

h2  = figure('Position',pos2);
plot(m,LFS);
xlabel('t','fontsize',16);
ylabel('Low-Frequency Component','fontsize',16);
title(['\omega = ',num2str(omega),', Iteration number = ',num2str(IterNo)...
    ', FileName: ',num2str(FileName1)]);

%Save the results using the tab-delimited format
button = questdlg('Do you want to save the results? (The files will be saved to the same folder with _HFS and _LFS appended to the end of the orginal filename)','Results','Yes');
if isequal(button,'Yes')
   fOutput1 = strrep(M1, '.txt', '_HFS.txt');
   dlmwrite(fOutput1,[m, HFS],'delimiter','\t', 'precision', 15);
   fOutput2 = strrep(M1, '.txt', '_LFS.txt');
   dlmwrite(fOutput2,[m, LFS'],'delimiter','\t', 'precision', 15);   
end

