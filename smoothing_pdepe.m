% @author Yuriy S. Polyakov and Serge F. Timashev
% 
% @copyright Copyright (c) 2005-2020, Yuriy S. Polyakov and Serge F. Timashev
% All rights reserved.
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 1. Redistributions of source code must retain the above copyright notice,
% this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. THIS SOFTWARE IS
% PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
% EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function [m, HFS, LFS] = smoothing_pdepe(omega,IterNo,M1);

%-----------------------------SMOOTHING------------------------------------
%omega - diffusion number
%IterNo - Number of iterations
%M1 - file name and path;
%--------------------------------------------------------------------------

%Loading the data of the first file into a matrix
V=dlmread(M1,'\t',0,0);

M = size(V,1);
V1 = V(:,2);

t = linspace(0,M-1,M);
tau = linspace(0,IterNo,11);

sol = pdepe(0,@pdex1pde,@pdex1ic,@pdex1bc,t,tau,[],V1,omega);
% Extract the first solution component as u.  This is not necessary
% for a single equation, but makes a point about the form of the output.
v = sol(:,:,1);

LFS = v(11,:)';
HFS = V(:,2) - LFS;
LFS = LFS';
m = V(:,1);

% --------------------------------------------------------------------------

function [c,f,s] = pdex1pde(t,tau,v,DuDt,V1,omega)
c = 1;
f = omega*DuDt;
s = 0;

% --------------------------------------------------------------------------

function u0 = pdex1ic(t,V1,omega)
u0 = V1(t+1);
    
% --------------------------------------------------------------------------

function [pl,ql,pr,qr] = pdex1bc(tl,vl,tr,vr,tau,V1,omega)
pl = 0;
ql = 1;
pr = 0;
qr = 1;